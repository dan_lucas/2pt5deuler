

		GPU Pseudospectral solver for the 2.5D model of the 3D Euler equations presented in the paper

		"Symmetry-plane model of 3D Euler flows and mapping to regular systems to improve blowup
		 assessment using numerical and analytical solutions" Mulungye, Lucas and Bustamante 2014.


____________________________________________________________________________________________________________________________


Outline of the code:

	   Code is of a standard pseudospectral form where timestepping occurs on fourier coefficients 
	   and nonlinear convolutions are computed in physical space. Timestepping scheme is RK4
	   (fourth order Runge-Kutta). Code is for use on NVIDIA GPUs via the CUDA programming API.
	   Use is made of the CUFFT library for the Fast Fourier Transform (FFT) on GPUs. We also
	   make use of the Thrust library of CUDA kernels for global reductions (spatial averages
	   and maxima). 

Please see the "README.txt" to accompany the original system (src_2pt5D/) and the mapped (src_map_2pt5D/)
in their respective source directories. bin/ contains input data, scripts and python code for post-processing.
Gnuplot scripts a_eps.gp and a_epslatex.gp create graphics of the physical data rewritten by rewrite_vort.f
adjustments are required for resolution and labelling. make_movie is a script to produce a gif movie of the
frames using a_eps.gp (a sed command relabels the time of each frame, this needs carefully updating for each case).
fit.py makes a linear fit by given time intervals of gamma_inf as outlined in the paper, input file needs including,
call using "python fit.py > fit1.dat"

  -----------
  Contacts
  -----------

        Authors: Dan Lucas
        Institution: School of Mathematical Sciences, University College Dublin
        Email:  dan.lucas@ucd.ie
	
18/09/14
