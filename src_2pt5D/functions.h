// functions used in 2.5D euler model (see main.cu and makefile)

// square<T> computes the square of a number f(x) -> x*x                                                                             
template <typename T>
struct squre
{
    __host__ __device__
    T operator()(const T& x) const {
      return x * x;
    }
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void read_params(double *lambda,double *Tstep, double *delta){
  // Read parameters from params.txt
  adaptFLAG=0; //in case adaptFLAG is forgotten
  char dum[10];
  FILE * params = fopen("params.txt", "r");

  fscanf(params," %s %lf \n", dum,lambda);
  printf(" read %s %lf \n",dum,*lambda);

  fscanf(params," %s %lf \n", dum,Tstep);
  printf(" read %s %lf \n",dum,*Tstep);

  fscanf(params," %s %lf \n", dum, delta);
  printf(" read %s %lf \n",dum,*delta);
  
  fscanf(params," %s %d \n", dum, &nx);
  printf(" read %s %d \n",dum,nx);

  fscanf(params," %s %d \n", dum, &ny);
  printf(" read %s %d \n",dum,ny);

  fscanf(params," %s %lf \n", dum, &Tout);
  printf(" read %s %lf \n",dum,Tout);

  fscanf(params," %s %d \n", dum, &adaptFLAG);
  printf(" read %s %d \n",dum,adaptFLAG);
  fclose(params);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void init(double2 *Z,double2 *G, double *kx,double *ky,double *LC,int *ikf,int *ikn,double *lambda){
  //intialisation for the run

  for(int ix=0; ix<iktx; ix++){
    for(int iy=0; iy<ikty; iy++){
      int it = ix+iktx*iy;
      // Set wave numbers
      kx[it] = double(ix);
      ky[it] = double(iy-kty);

      double wk= sqrt(kx[it]*kx[it]+ky[it]*ky[it]);
      // set dealiasing (Hou filter)
      LC[it] = exp(-36.0*(pow(2*kx[it]/double(nx),36.0))-36.0*(pow(2.0*ky[it]/double(ny),36.0)));

      //uncomment here for 2/3 dealiasing
/*       LC[it]=1.0;  */
/*       if( wk > kty){// || kx[it]==0 && ky[it] <= 0 ){ */
/* 	LC[it]=0.0; */
/*       } */
      
      // Set Initial condition, IN FOURIER!!
      if(*lambda==0){
	if(kx[it]==1 && ky[it]==1){
          Z[it].x=-0.25;
          Z[it].y= 0.0;

          G[it].x=-0.25;
          G[it].y= 0.0;
        }else if(kx[it]==1 && ky[it]==-1){
          Z[it].x= 0.25;
          Z[it].y= 0.0;

          G[it].x= 0.25;
          G[it].y= 0.0;
        }else{
          Z[it].x= 0.0;
          Z[it].y= 0.0;

          G[it].x= 0.0;
          G[it].y= 0.0;
        }
      }else{
	if(kx[it]==1 && ky[it]==1){
	  Z[it].x=-0.25;
	  Z[it].y= 0.0;
	  
	  G[it].x=-0.25;
	  G[it].y= 0.0;
	}else if(kx[it]==1 && ky[it]==-1){
	  Z[it].x=-0.25;
	  Z[it].y= 0.0;
	  
	  G[it].x= 0.25;
	  G[it].y= 0.0;
	}else if(kx[it]==0 && ky[it]==1){
	  Z[it].x= 0.0;
	  Z[it].y= 0.0;
	  
	  G[it].x=-0.5;
	  G[it].y= 0.0;
	}else if(kx[it]==0 && ky[it]==-1){
	  Z[it].x= 0.0;
	  Z[it].y= 0.0;
	  
	  G[it].x=-0.5;
	  G[it].y= 0.0;
	}else if(kx[it]==1 && ky[it]==0){
	  Z[it].x= 0.0;
	  Z[it].y= 0.5;
	  
	  G[it].x= 0.0;
	  G[it].y= 0.0;
	}else{
	  Z[it].x= 0.0;
	  Z[it].y= 0.0;
	  
	  G[it].x= 0.0;
	  G[it].y= 0.0;
	}
      }

    }
  }

  //set FFT padding index arrays
  for(int ix=0; ix<iktx; ix++){
    for(int iy=0; iy<kty+1; iy++){
      int it = ix+nx2*iy;
      ikf[it] = ix + iktx*(iy+kty);
    }
    for(int iy=kty+1; iy<(ny-kty); iy++){
      int it = ix+nx2*iy;
      ikf[it] = -1;
    }
    for(int iy=(ny-kty); iy<ny; iy++){
      int it = ix+nx2*iy;
      ikf[it] = ix+iktx*(iy+kty-ny);
    }
  }
  for(int ix=iktx; ix<nx2; ix++){
    for(int iy=0; iy<ny; iy++){
      int it = ix+nx2*iy;
      ikf[it] = -1;
    }
  }
  for(int it=0; it<nkt; it++){
    ikn[it] = 0;
  }

  for(int ix=0; ix<iktx; ix++){
    for(int iy=0; iy<kty; iy++){
      int it = ix+iktx*iy;
      ikn[it] = ix + nx2*(iy+ny-kty);
    }
    for(int iy=kty; iy<ikty; iy++){
      int it = ix+iktx*iy;
      ikn[it] = ix + nx2*(iy-kty);
    }
  }  
}
  
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void KR_FFT_ALL(cufftDoubleComplex *d_ZK,cufftDoubleComplex *d_GK, cufftDoubleComplex *d_UK,cufftDoubleComplex *d_VK, cufftDoubleReal*d_NZR,cufftHandle PlanZ2D,int *d_ikF){
  cufftDoubleComplex *d_FF;

  //This version does each batch of 4 FFTs so output is NZR = [G,Z,U,V] in that order (see FFN)

  (cudaMalloc((void**)&d_FF,sizeof(cufftDoubleComplex)*4*(ny)*(nx2)));

  // older cuda versions require conjugate symmetry imposed for spec-phys transforms
  //  conj<<<nblocks,nthreads>>>(d_GK,d_ZK,d_UK,d_VK,d_kx,d_ky);
  setFFN<<<nblocks,nthreads>>>(d_GK,d_ZK,d_UK,d_VK,d_FF,d_ikF);  // Pad truncated wave numbers

  (cufftExecZ2D(PlanZ2D,d_FF,d_NZR));  // Do Z FFT

  (cudaFree(d_FF));

 }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////

void RK_FFT(cufftDoubleComplex *d_NZK,cufftDoubleComplex *d_G2K, cufftDoubleReal*d_NZR, cufftDoubleReal*d_G2R,cufftHandle PlanBatchD2Z,cufftHandle PlanD2Z,int *d_ikN){
  cufftDoubleComplex *d_FF1,*d_FF2;

  //Doing a batch of 4 2D FFTs so that now NZK stores UG, VG,UZ,VZ in that order
  //Also need the gamma^2 term (G2R->G2K)

  (cudaMalloc((void**)&d_FF1,sizeof(cufftDoubleComplex)*4*(ny)*(nx2)));
  (cudaMalloc((void**)&d_FF2,sizeof(cufftDoubleComplex)*(ny)*(nx2)));

  (cufftExecD2Z(PlanBatchD2Z,d_NZR,d_FF1));
  (cufftExecD2Z(PlanD2Z,d_G2R,d_FF2));

  normFF1<<<nblocks,nthreads>>>(d_NZK,d_G2K,d_FF1,d_FF2,d_ikN);  // normalise and unpack output

  (cudaFree(d_FF1));
  (cudaFree(d_FF2));
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////

void RK_FFT_Z(cufftDoubleComplex *d_ZK, cufftDoubleReal*d_ZR,cufftHandle PlanD2Z,int *d_ikN){
  cufftDoubleComplex *d_FF;

  // routine to do a one-off transform (for example if physical initial data is provided)

  (cudaMalloc((void**)&d_FF,sizeof(cufftDoubleComplex)*(ny)*(nx2)));

  (cufftExecD2Z(PlanD2Z,d_ZR,d_FF));

  normFFZ<<<nblocks,nthreads>>>(d_ZK,d_FF,d_ikN);  // normalise output

  (cudaFree(d_FF));
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////
 void interpolate_find(double *GI, double& Gmax,double& xgmax, double& ygmax){

   // This function locates an interpolated maximum via the P_6k "quarter-section" method
   // presented in MLB 2014. (6 point cubic spline on 1/4 refined region about a current max)
   //Inputs are a 7x7 array about the collocation max (3,3)
   int ixx,iyy;
   double x1,y1,wx0,wx1,wx2,wx3,wx4,wx5,wy0,wy1,wy2,wy3,wy4,wy5;
   double ddx=1.0;
   double ddy=1.0;
   double xx=0.0;
   double yy=0.0;
   double xx2=0.0;
   double yy2=0.0;

   Gmax=0.0;
   xgmax=0.0;
   ygmax=0.0;
   // Outer loop does the sectioning iterations
   for(int ib=0; ib<9; ib++){
     //refine by a quarter
     ddx *= 0.25;
     ddy *= 0.25;
     // loop over the 3 new neighbours in each direction
     for(int ix=0; ix<7; ix++){
       double xx1=xx+(ix-3)*ddx;
       if(xx1 < -1.0){
	 continue;
       }else if(xx1 < 0.0){
	 x1 = 1.0+xx1;
	 ixx = 2;
       }else{
	 x1 = xx1;
	 ixx = 3;
       }
       //set x direction interpolation weights
       double x12=x1*x1;
       double x13=x1*x1*x1;
       wx0 = 7.0*x1/90.0 - 2.0*(x12)/15.0 + (x13)/18.0;
       wx1 = -28.0*x1/45.0 + 16.0*(x12)/15.0 - 4.0*(x13)/9.0;
       wx2 = 1.0 - 11.0*x1/90.0 - 29.0*x12/15.0 + 19.0*x13/18.0;
       wx3 = 37.0*x1/45.0 + 37.0*x12/30.0 - 19.0*x13/18.0;
       wx4 = -8.0*x1/45.0 - 4.0*x12/15.0 + 4.0*x13/9.0;
       wx5 = x1/45.0 + x12/30.0 - x13/18.0;
       for(int iy=0; iy<7; iy++){
	 double yy1=yy+(iy-3)*ddy;
	 if(yy1 < -1.0){
	   continue;
	 }else if(yy1 < 0.0){
	   y1 = 1.0+yy1;
	   iyy = 2;
	 }else{
	   y1 = yy1;
	   iyy = 3;
	 }
	 //set y direction interpolation weights
	 double y12=y1*y1;
	 double y13=y1*y1*y1;
	 wy0 = 7.0*y1/90.0 - 2.0*(y12)/15.0 + (y13)/18.0;
	 wy1 = -28.0*y1/45.0 + 16.0*(y12)/15.0 - 4.0*(y13)/9.0;
	 wy2 = 1.0 - 11.0*y1/90.0 - 29.0*y12/15.0 + 19.0*y13/18.0;
	 wy3 = 37.0*y1/45.0 + 37.0*y12/30.0 - 19.0*y13/18.0;
	 wy4 = -8.0*y1/45.0 - 4.0*y12/15.0 + 4.0*y13/9.0;
	 wy5 = y1/45.0 + y12/30.0 - y13/18.0;

	 // new interpolated value (full weights are convolution in each direction)
	 double gint = wx0*wy0*GI[ixx-2+(iyy-2)*7]+
	   wx0*wy1*GI[ixx-2+(iyy-1)*7]+
	   wx0*wy2*GI[ixx-2+(iyy  )*7]+
	   wx0*wy3*GI[ixx-2+(iyy+1)*7]+
	   wx0*wy4*GI[ixx-2+(iyy+2)*7]+
	   wx0*wy5*GI[ixx-2+(iyy+3)*7]+
	   wx1*wy0*GI[ixx-1+(iyy-2)*7]+
	   wx1*wy1*GI[ixx-1+(iyy-1)*7]+
	   wx1*wy2*GI[ixx-1+(iyy  )*7]+
	   wx1*wy3*GI[ixx-1+(iyy+1)*7]+
	   wx1*wy4*GI[ixx-1+(iyy+2)*7]+
	   wx1*wy5*GI[ixx-1+(iyy+3)*7]+
	   wx2*wy0*GI[ixx  +(iyy-2)*7]+
	   wx2*wy1*GI[ixx  +(iyy-1)*7]+
	   wx2*wy2*GI[ixx  +(iyy  )*7]+
	   wx2*wy3*GI[ixx  +(iyy+1)*7]+
	   wx2*wy4*GI[ixx  +(iyy+2)*7]+
	   wx2*wy5*GI[ixx  +(iyy+3)*7]+
	   wx3*wy0*GI[ixx+1+(iyy-2)*7]+
	   wx3*wy1*GI[ixx+1+(iyy-1)*7]+
	   wx3*wy2*GI[ixx+1+(iyy  )*7]+
	   wx3*wy3*GI[ixx+1+(iyy+1)*7]+
	   wx3*wy4*GI[ixx+1+(iyy+2)*7]+
	   wx3*wy5*GI[ixx+1+(iyy+3)*7]+
	   wx4*wy0*GI[ixx+2+(iyy-2)*7]+
	   wx4*wy1*GI[ixx+2+(iyy-1)*7]+
	   wx4*wy2*GI[ixx+2+(iyy  )*7]+
	   wx4*wy3*GI[ixx+2+(iyy+1)*7]+
	   wx4*wy4*GI[ixx+2+(iyy+2)*7]+
	   wx4*wy5*GI[ixx+2+(iyy+3)*7]+
	   wx5*wy0*GI[ixx+3+(iyy-2)*7]+
	   wx5*wy1*GI[ixx+3+(iyy-1)*7]+
	   wx5*wy2*GI[ixx+3+(iyy  )*7]+
	   wx5*wy3*GI[ixx+3+(iyy+1)*7]+
	   wx5*wy4*GI[ixx+3+(iyy+2)*7]+
	   wx5*wy5*GI[ixx+3+(iyy+3)*7];

	 // check if the new point is larger
	 if(abs(gint)>abs(Gmax)){
	   Gmax=gint;
	   xx2=xx1;
	   yy2=yy1;
	 }
       }
     }
     //update the current maximum position
     xx=xx2;
     yy=yy2;
   }
   //finalise the position
   xgmax=xx;
   ygmax=yy;
 }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////

 void interpolate_point(double *GI, double& Gmax,double xgmax, double ygmax){
   // This subroutine computes the value of the field at a GIVEN point (xgamx,ygmax)
   // using the same 6 point cubic spline interpolation as in interpolate_find

   int ixx,iyy;
   double x1,y1,wx0,wx1,wx2,wx3,wx4,wx5,wy0,wy1,wy2,wy3,wy4,wy5;
   Gmax=0.0;
   if(xgmax < 0.0){
     x1 = 1.0+xgmax;
     ixx = 2;
   }else{
     x1 = xgmax;
     ixx = 3;
   }
   // x weights
   double x12=x1*x1;
   double x13=x1*x1*x1;
   wx0 = 7.0*x1/90.0 - 2.0*(x12)/15.0 + (x13)/18.0;
   wx1 = -28.0*x1/45.0 + 16.0*(x12)/15.0 - 4.0*(x13)/9.0;
   wx2 = 1.0 - 11.0*x1/90.0 - 29.0*x12/15.0 + 19.0*x13/18.0;
   wx3 = 37.0*x1/45.0 + 37.0*x12/30.0 - 19.0*x13/18.0;
   wx4 = -8.0*x1/45.0 - 4.0*x12/15.0 + 4.0*x13/9.0;
   wx5 = x1/45.0 + x12/30.0 - x13/18.0;
   
   if(ygmax < 0.0){
     y1 = 1.0+ygmax;
     iyy = 2;
   }else{
     y1 = ygmax;
     iyy = 3;
   }
   //y weights
   double y12=y1*y1;
   double y13=y1*y1*y1;
   wy0 = 7.0*y1/90.0 - 2.0*(y12)/15.0 + (y13)/18.0;
   wy1 = -28.0*y1/45.0 + 16.0*(y12)/15.0 - 4.0*(y13)/9.0;
   wy2 = 1.0 - 11.0*y1/90.0 - 29.0*y12/15.0 + 19.0*y13/18.0;
   wy3 = 37.0*y1/45.0 + 37.0*y12/30.0 - 19.0*y13/18.0;
   wy4 = -8.0*y1/45.0 - 4.0*y12/15.0 + 4.0*y13/9.0;
   wy5 = y1/45.0 + y12/30.0 - y13/18.0;
   
   // interpolated point
   Gmax = wx0*wy0*GI[ixx-2+(iyy-2)*7]+
     wx0*wy1*GI[ixx-2+(iyy-1)*7]+
     wx0*wy2*GI[ixx-2+(iyy  )*7]+
     wx0*wy3*GI[ixx-2+(iyy+1)*7]+
     wx0*wy4*GI[ixx-2+(iyy+2)*7]+
     wx0*wy5*GI[ixx-2+(iyy+3)*7]+
     wx1*wy0*GI[ixx-1+(iyy-2)*7]+
     wx1*wy1*GI[ixx-1+(iyy-1)*7]+
     wx1*wy2*GI[ixx-1+(iyy  )*7]+
     wx1*wy3*GI[ixx-1+(iyy+1)*7]+
     wx1*wy4*GI[ixx-1+(iyy+2)*7]+
     wx1*wy5*GI[ixx-1+(iyy+3)*7]+
     wx2*wy0*GI[ixx  +(iyy-2)*7]+
     wx2*wy1*GI[ixx  +(iyy-1)*7]+
     wx2*wy2*GI[ixx  +(iyy  )*7]+
     wx2*wy3*GI[ixx  +(iyy+1)*7]+
     wx2*wy4*GI[ixx  +(iyy+2)*7]+
     wx2*wy5*GI[ixx  +(iyy+3)*7]+
     wx3*wy0*GI[ixx+1+(iyy-2)*7]+
     wx3*wy1*GI[ixx+1+(iyy-1)*7]+
     wx3*wy2*GI[ixx+1+(iyy  )*7]+
     wx3*wy3*GI[ixx+1+(iyy+1)*7]+
     wx3*wy4*GI[ixx+1+(iyy+2)*7]+
     wx3*wy5*GI[ixx+1+(iyy+3)*7]+
     wx4*wy0*GI[ixx+2+(iyy-2)*7]+
     wx4*wy1*GI[ixx+2+(iyy-1)*7]+
     wx4*wy2*GI[ixx+2+(iyy  )*7]+
     wx4*wy3*GI[ixx+2+(iyy+1)*7]+
     wx4*wy4*GI[ixx+2+(iyy+2)*7]+
     wx4*wy5*GI[ixx+2+(iyy+3)*7]+
     wx5*wy0*GI[ixx+3+(iyy-2)*7]+
     wx5*wy1*GI[ixx+3+(iyy-1)*7]+
     wx5*wy2*GI[ixx+3+(iyy  )*7]+
     wx5*wy3*GI[ixx+3+(iyy+1)*7]+
     wx5*wy4*GI[ixx+3+(iyy+2)*7]+
     wx5*wy5*GI[ixx+3+(iyy+3)*7];
   
 }
