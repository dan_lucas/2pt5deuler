      program read
C
C READS IN BINARY PHYSICAL DATA AND WRITES FORMATTED FRAME FOR 
C VISUALISATION
C
      IMPLICIT NONE
      INTEGER, PARAMETER  :: rk = 8      ! floating point precision      
      INTEGER, PARAMETER ::NALL=4096
      INTEGER :: ikx,iky,nt,nw,NX,NY
      CHARACTER*40 dfile
C ----------------------------------------------------------------
      REAL(rk) :: ZR(NALL,NALL),GR(NALL,NALL),time

c
      print*,'What data file? (default vort.dat)'
      read(5,'(A)') dfile 
      if (dfile(1:1) .eq. ' ') dfile= 'vort.dat'      

      print*,'What resolution (NX,NY)?'
      read(5,*) NX,NY
      print*,'Which output?'
      read(5,*) nw
      print*,'You have selected number ',nw
c
      open(12,file=dfile,access="STREAM",form='UNFORMATTED')
      open(13,file='vort_a.dat',form='FORMATTED')
      open(14,file='gamma_a.dat',form='FORMATTED')
c
c vort.dat is the model output file, vort_a.dat is the rewritten ascii version.
c
      do nt=1,10000
         read(12) time,((ZR(ikx,iky),ikx=1,NX),iky=1,NY),
     .        ((GR(ikx,iky),ikx=1,NX),iky=1,NY)
         if (nt.eq.nw) then
            do iky=1,NY
               write(13,333) (ZR(ikx,iky),ikx=1,NX)
               write(13,*) '           '
               write(14,333) (GR(ikx,iky),ikx=1,NX)
               write(14,*) '           '
            enddo
            print*,'time = ',time
            stop
         endif
      enddo
C     
 999  stop
 333  format(1x,E12.5,1x)
      END
C     
