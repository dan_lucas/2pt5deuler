///////////////////////////////////////////////////////////////////////////////////////////////
/*
  THIS IS THE MAIN FILE FOR THE PSEUDOSPECTRAL 2.5D EULER MODEL FROM 
  MULUNGYE, LUCAS & BUSTAMANTE (2014) WRITTEN IN CUDA FOR USE ON NVIDIA GPU CARDS.
  CODE WRITTEN BY DAN LUCAS SEPTEMBER 2014 AT UCD, DUBLIN.

  PSEUDOSPECTRAL STRUCTURE BORROWED FROM NAVIER-STOKES SOLVER WRITTEN BY THE SAME AUTHOR
  USED IN LUCAS & KERSWELL (JFM 2014).

  CURRENT VERSION VALIDATED FOR CUDA 5.5

  DEPENDENCIES AS BELOW AND IN MAKEFILE. SEE README FOR MORE INFO.


 */
////////////////////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <helper_cuda.h>
#include <cufft.h>
#include <cuda_runtime.h>
#include <thrust/functional.h>
#include <thrust/transform_reduce.h>
#include <thrust/reduce.h>
#include <thrust/extrema.h>
#include <thrust/device_vector.h>
#include "kernels.h"
#include "functions.h"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MAIN TIMESTEPPING ROUTINE
void timeStep(double2 *Z, //VORTICITY
	      double2 *G, //GAMMA
	      double *kx, //X WAVENUMBERS
	      double *ky, //Y WAVENUMBERS
	      double *LC, //DEALIASING MASK
	      double *Tstep, // TIME DURATION
	      double *delta, // TIMESTEP
	      double *lambda,// LAMBDA PARAMETER
	      int *ikF, // FFT PADDING INDEXING
	      int *ikN) // FFT UNPADDING INDEXING
{
  
  printf("\n In timestep \n");

  // open output files, named with resolution
  char statsfile[sizeof "stats_0000.dat"];
  sprintf(statsfile, "stats_%04d.dat",nx);
  char errfile[sizeof "err_0000.dat"];
  sprintf(errfile, "err_%04d.dat",nx);
  char specfile[sizeof "spec_00.dat"];
  sprintf(specfile, "spec_%04d.dat",nx);
  char ensfile[sizeof "vort_00.dat"];
  sprintf(ensfile, "vort_%04d.dat",nx);

  vort = fopen(ensfile,"wb");
  points = fopen(specfile,"w");
  stats = fopen(statsfile,"w");
  errf = fopen(errfile,"w");

  int nOut=int(Tout);
  int timestep=0;
  int nG0=5000000;
  int nT0=500000;
  int it=0;
  int ig=0;
  double *time0,*tauT0,*tauG0,*Gam0;
  // For the case lambda=0 read in the accompanying data files
  // which contain t, tau and SUP(gamma) from S ODE for comparison 
  // to full PDE integration.
  if(*lambda==0.0){
    time0=(double*)malloc(sizeof(double)*nT0);
    tauT0=(double*)malloc(sizeof(double)*nT0);
    tauG0=(double*)malloc(sizeof(double)*nG0);
    Gam0=(double*)malloc(sizeof(double)*nG0);
    FILE * tfile=fopen("t_tau_lambda0.txt","r");
    FILE * gfile=fopen("G_tau_lambda0.txt","r");
    double dum1,dum2,dum3;
    for(int i=0; i<nT0; i++){
      fscanf(tfile," %le %le\n",&dum1,&dum2);
      tauT0[i]=dum1;
      time0[i]=dum2;
    }
    for(int i=0; i<nG0; i++){
      fscanf(gfile," %le %le\n",&dum1,&dum3);
      tauG0[i]=dum1;
      Gam0[i]=dum3;
    }
    fclose(tfile);
    fclose(gfile);
  }else if(*lambda != -1.5){
    printf("\n ******** WARNING: \n YOUR CHOICE OF LAMBDA HAS NO EXACT SOLUTION CODED FOR COMPARISON TO NUMERICAL SOLUTION *****************  \n (errors output will be meaningless!)");
  }

  // define some host variables
  double *ZR,*GR;
  double avG2,Gmax,Zmax,delt2,anaG,anaZ,tau;
  double Ga2,Gn2,Ge2, Oa2,On2,Oe2;
  size_t avail,total;
  float time;
  cudaEvent_t cpu1, cpu2;
  ZR=(double*)malloc(sizeof(double)*nr);
  GR=(double*)malloc(sizeof(double)*nr);
  cudaEventCreate(&cpu1);
  cudaEventCreate(&cpu2);
  // Define global device variables
  int *d_ikF, *d_ikN;
  double *d_avG2,*d_LL;
  cufftDoubleComplex *d_Z,*d_Z0,*d_Z1,*d_Z2,*d_Z3;
  cufftDoubleComplex *d_G,*d_G0,*d_G1,*d_G2,*d_G3;
  cufftDoubleComplex  *d_UK, *d_VK,*d_NZK;
  cufftDoubleReal *d_G2R,*d_NZR;
  cufftHandle PlanBatchZ2D,PlanBatchD2Z,PlanD2Z;

  // Allocate global memory on GPU. (Constant memory does not need allocating) 	
  (cudaMalloc((void**)&d_Z,sizeof(cufftDoubleComplex)*nkt));
  (cudaMalloc((void**)&d_Z0,sizeof(cufftDoubleComplex)*nkt));
  (cudaMalloc((void**)&d_Z1,sizeof(cufftDoubleComplex)*nkt));
  (cudaMalloc((void**)&d_Z2,sizeof(cufftDoubleComplex)*nkt));
  (cudaMalloc((void**)&d_Z3,sizeof(cufftDoubleComplex)*nkt));

  (cudaMalloc((void**)&d_G,sizeof(cufftDoubleComplex)*nkt));
  (cudaMalloc((void**)&d_G0,sizeof(cufftDoubleComplex)*nkt));
  (cudaMalloc((void**)&d_G1,sizeof(cufftDoubleComplex)*nkt));
  (cudaMalloc((void**)&d_G2,sizeof(cufftDoubleComplex)*nkt));
  (cudaMalloc((void**)&d_G3,sizeof(cufftDoubleComplex)*nkt));

  (cudaMalloc((void**)&d_UK,sizeof(cufftDoubleComplex)*nkt));
  (cudaMalloc((void**)&d_VK,sizeof(cufftDoubleComplex)*nkt));

  (cudaMalloc((void**)&d_NZK,sizeof(cufftDoubleComplex)*4*nkt));
  
  (cudaMalloc((void**)&d_NZR,sizeof(cufftDoubleReal)*4*nr));
  (cudaMalloc((void**)&d_G2R,sizeof(cufftDoubleReal)*nr));
  
  (cudaMalloc((void**)&d_kx,sizeof(double)*nkt));
  (cudaMalloc((void**)&d_ky,sizeof(double)*nkt));
  (cudaMalloc((void**)&d_LL,sizeof(double)*nkt));
  (cudaMalloc((void**)&d_ikF,sizeof(int)*n2));
  (cudaMalloc((void**)&d_ikN,sizeof(int)*nkt));
  (cudaMalloc((void**)&d_avG2,sizeof(double)));
  
  // Copy state data to GPU global memory 
  (cudaMemcpy(d_Z,Z,sizeof(cufftDoubleComplex)*nkt,cudaMemcpyHostToDevice));
  (cudaMemcpy(d_G,G,sizeof(cufftDoubleComplex)*nkt,cudaMemcpyHostToDevice));
  
  // Copy constant parameters to GPU constant memory 
  (cudaMemcpyToSymbol(d_DELTA,delta,sizeof(double)));
  (cudaMemcpyToSymbol(d_LAM,lambda,sizeof(double)));
  (cudaMemcpyToSymbol(d_IN,&in,sizeof(double)));
  (cudaMemcpyToSymbol(d_IKTX,&iktx,sizeof(int)));
  (cudaMemcpyToSymbol(d_IKTY,&ikty,sizeof(int)));
  (cudaMemcpyToSymbol(d_KTY,&kty,sizeof(int)));
  (cudaMemcpyToSymbol(d_NX,&nx,sizeof(int)));
  (cudaMemcpyToSymbol(d_NY,&ny,sizeof(int)));
  (cudaMemcpyToSymbol(d_OR,&nr,sizeof(int)));
  (cudaMemcpyToSymbol(d_OK,&nkt,sizeof(int)));
  (cudaMemcpyToSymbol(d_O2,&n2,sizeof(int)));

  //Set up various arrays to enable generic kernel calls
  //i.e. indexing for padding either side of FFTs, wavenumber arrays, mask, and timestep arrays.
  // This must be done on CPU for scalability (large problems violate max threads per block)
  (cudaMemcpy(d_kx,kx,sizeof(double)*nkt,cudaMemcpyHostToDevice));
  (cudaMemcpy(d_ky,ky,sizeof(double)*nkt,cudaMemcpyHostToDevice));
  (cudaMemcpy(d_LL,LC,sizeof(double)*nkt,cudaMemcpyHostToDevice));
  (cudaMemcpy(d_ikF,ikF,sizeof(int)*n2,cudaMemcpyHostToDevice));
  (cudaMemcpy(d_ikN,ikN,sizeof(int)*nkt,cudaMemcpyHostToDevice));
  //Set FFT Plans: balance FFT work, batch 4 FFTs in each direction
  // plus one extra for gamma^2
  (cufftPlan2d(&PlanD2Z,ny,nx,CUFFT_D2Z));
  (cufftPlanMany(&PlanBatchD2Z,2,np,NULL,1,0,NULL,1,0,CUFFT_D2Z,4));
  (cufftPlanMany(&PlanBatchZ2D,2,np,NULL,1,0,NULL,1,0,CUFFT_Z2D,4));
  // Cast thrust pointers for computing average and finding maximum
  thrust::device_ptr<double> d_NZRptr(d_NZR);
  thrust::device_ptr<double> d_G2Rptr(d_G2R);

  // Do a check of global memory use
  avail =0;
  total = 0;
  cudaMemGetInfo(&avail,&total);
  printf("\n total : %f MB \n",double(total)/(1024.0f*1024.0f));
  printf("\n avail : %f MB \n",double(avail)/(1024.0f*1024.0f));
  printf("\n used : %f MB \n",double(total-avail)/(1024.0f*1024.0f));

  // Do an initial set of velocity coeffs. Subsequently this occurs at the end of stepping kernels
  setVelocity<<<nblocks,nthreads>>>(d_Z,d_G,d_Z0,d_G0,d_UK,d_VK,d_kx,d_ky);
  delt2=*delta;
  // intialise cumulative quantities
  tme=0.0;
  Ga2=0.0;
  Gn2=0.0;
  Ge2=0.0;
  Oa2=0.0;
  On2=0.0;
  Oe2=0.0;
  // **************************
  //STEPPING STARTS HERE
  // **************************
  cudaEventRecord( cpu1, 0 );

  while(tme<=*Tstep){
    timestep++;

    KR_FFT_ALL(d_Z0,d_G0,d_UK,d_VK,d_NZR,PlanBatchZ2D,d_ikF);
    SqrReal<<<nblocks,nthreads>>>(d_G2R,d_NZR);    // Do real space square (needed for average)
    avG2 = (thrust::reduce(d_G2Rptr,d_G2Rptr+nr,(double) 0.0,thrust::plus<double>()))*in;// use thrust reduce for average
    (cudaMemcpy(d_avG2,&avG2, sizeof(double), cudaMemcpyHostToDevice));//cpy average back to device

    //    double tcrit=Tout*double(int((tme+small)/Tout));//want outputs every Tout!
    //    if((tme-delt2-tcrit+small)*(tme+small-tcrit) < 0.0 & tme > 1.4179){//sporadic outputs of physical field and spectrum of gamma
    if((timestep % nOut) == 0){
      (cudaMemcpy(G,d_G,sizeof(cufftDoubleComplex)*nkt,cudaMemcpyDeviceToHost));
      /*(cudaMemcpy(ZR,d_NZR+nr,sizeof(cufftDoubleReal)*nr,cudaMemcpyDeviceToHost));
	(cudaMemcpy(GR,d_NZR,sizeof(cufftDoubleReal)*nr,cudaMemcpyDeviceToHost));
	fwrite(&tme,sizeof(double),1,vort);
	fwrite(ZR,sizeof(cufftDoubleReal),nr,vort);
	fwrite(GR,sizeof(cufftDoubleReal),nr,vort);
	fflush(vort);*/

	double spec[ikty];
	int size = 0;
	for(int i=0; i<ikty; i++) spec[i]=0.0;
	for(int i=0; i<nkt; i++){
	  if(LC[i] != 0.0 ){
	    double wk = 1.0/max(kx[i]*kx[i] + ky[i]*ky[i],0.001);
	    double VZ = G[i].x*G[i].x + G[i].y*G[i].y;
	    int j = int(sqrt(1.0/wk)+0.5)-1;
	    spec[j] += VZ;
	    size = max(size,j);
	  }
	}

	for(int j=0; j<size; j++){
	  fprintf(points,"%d %e \n",j,spec[j]);
	}
	fprintf(points,"  \n");
    }

    //find location of max of gamma
    // first thrust does the global reduction to find the max position
      thrust::device_ptr<double> res = thrust::max_element(d_G2Rptr,d_G2Rptr+nr);
      double xgmax,ygmax;
      int imax = res-d_G2Rptr;
      double rcheck=*res;
      int iymax= int(imax/nx);
      int ixmax= imax-iymax*nx;
      double GI[49];
      double ZI[49];
      // reduce memcpy by only pulling off the 7X7 collocation points needed for interpolation
      // future work could implement this on gpu?
      for(int ikx=ixmax-3; ikx<ixmax+4; ikx++){
	int ixx=ikx;
	if(ikx<0) ixx+=nx;
	if(ikx>nx) ixx-=nx;
	for(int iky=iymax-3; iky<iymax+4; iky++){
	  int iyy=iky;
	  if(iky<0 ) iyy+=ny;
	  if(iky>ny) iyy-=ny;
	  int ip1 = (ikx-ixmax+3)+(iky-iymax+3)*7;
	  int ip2 = ixx+iyy*nx;
	  double dummy;

	  (cudaMemcpy(&dummy,d_NZR+ip2,sizeof(cufftDoubleReal),cudaMemcpyDeviceToHost));
	  GI[ip1]=dummy;
	  (cudaMemcpy(&dummy,d_NZR+ip2+nr,sizeof(cufftDoubleReal),cudaMemcpyDeviceToHost)); //get vort too
	  ZI[ip1]=dummy;
	}
      }
      xgmax=ixmax*twopi/double(nx);
      ygmax=iymax*twopi/double(ny);
      interpolate_find(GI,Gmax,xgmax,ygmax);//find max gamma and its location xgmax,ygmax
      interpolate_point(ZI,Zmax,xgmax,ygmax);// find vorticity at xgmax,ygmax
      xgmax = (ixmax+xgmax)*(twopi/double(nx));
      ygmax = (iymax+ygmax)*(twopi/double(ny));

      //compute errors compared to analytic expression (or S ODE solution)
      // only coded for lambda=-3/2 and lamba=0 cases, be careful of output for other cases      
      if(*lambda==-1.5){
	anaG =(2*Cfac*tan(Cfac*tme)+root2/(cos(Cfac*tme)*cos(Cfac*tme)*(1.0-tan(Cfac*tme)/(root2*Cfac))));
	anaZ =1.0/(cos(Cfac*tme)*cos(Cfac*tme)*(1.0-2.0*(sqrt(2.0/3.0))*tan(Cfac*tme))*(1.0-2.0*(sqrt(2.0/3.0))*tan(Cfac*tme)));
	tau = log(1.0/(pow(cos(Cfac*tme)-2.0*(sqrt(2.0/3.0))*sin(Cfac*tme),2)));
      }else if(*lambda==0.0){
	if(tme==0.0 || tau>5.0){
	  anaG=1.0;
	  tau=0.0;
	}else{
	  while(time0[it]<tme){
	    it++;
	  }
	  tau=(tauT0[it]*(tme-time0[it-1])+tauT0[it-1]*(time0[it]-tme))/(time0[it]-time0[it-1]);
	  while(tauG0[ig]<tauT0[it]){
	    ig++;
	  }
	  anaG=(Gam0[ig]*(tau-tauG0[ig-1])+Gam0[ig-1]*(tauG0[ig]-tau))/(tauG0[ig]-tauG0[ig-1]);
	}
	//	printf("%d %d \n",it,ig);
	anaZ=1.0; // don't have omega...
      }else{
	anaG=1.0;// don't have any error measure
	anaZ=1.0;
	tau=tme;
      }
      //relative error in gamma and omega:
      double errZ = abs(Zmax)/anaZ -1.0;
      double errG = abs(Gmax)/anaG-1.0;
      // compute L2 norms for Q_gamma and Q_omega
      Ga2 +=anaG*anaG;
      Gn2 +=Gmax*Gmax;
      Ge2 +=pow(abs(Gmax)-anaG,2);
      Oa2 +=anaZ*anaZ;
      On2 +=Zmax*Zmax;
      Oe2 +=pow(abs(Zmax)-anaZ,2);
      cudaThreadSynchronize();
      cudaEventRecord( cpu2, 0 );
      cudaEventSynchronize( cpu2 );
      cudaEventElapsedTime( &time, cpu1, cpu2 );
      double fdot = sgn(Gmax)*((2+*lambda)*avG2-(1+*lambda)*Gmax*Gmax);
      double errGsq = abs(avG2/0.75 -1.0);
      //      printf("%e %e %e %e\n",tme,Gmax,anaG,errG);
      fflush(stdout);
      fprintf(stats,"%.15e %.15e %e %.15e %e %.15e %e\n",delt2,tme,time,Gmax,Zmax,avG2,fdot);
      fprintf(errf,"%e %e %e %e %e %e %e %e \n",tau,tme,time,abs(errG),abs(errZ),errGsq,Ge2/(Ga2+Gn2),Oe2/(Oa2+On2));
      fflush(points);
      fflush(stats);
      fflush(errf);
      if(adaptFLAG==1){
	delt2=*delta/abs(Gmax);
	(cudaMemcpyToSymbol(d_DELTA,&delt2,sizeof(double)));
      }


    convol<<<nblocks,nthreads>>>(d_G2R,d_NZR,d_avG2);    // Do real space convolution
    RK_FFT(d_NZK,d_UK,d_NZR,d_G2R,PlanBatchD2Z,PlanD2Z,d_ikN);    // RK does a batch of 4+1 ffts
    Step1<<<nblocks,nthreads>>>(d_Z,d_G,d_Z0,d_G0,d_Z1,d_G1,d_NZK,d_UK,d_VK,d_kx,d_ky,d_LL); // first RK4 mid step
    //structure repeats now:

    KR_FFT_ALL(d_Z0,d_G0,d_UK,d_VK,d_NZR,PlanBatchZ2D,d_ikF);
    SqrReal<<<nblocks,nthreads>>>(d_G2R,d_NZR);    // Do real space square (needed for average)
    avG2 = thrust::reduce(d_G2Rptr,d_G2Rptr+nr,(double) 0.0,thrust::plus<double>())*in;
    (cudaMemcpy(d_avG2,&avG2, sizeof(double), cudaMemcpyHostToDevice));

    convol<<<nblocks,nthreads>>>(d_G2R,d_NZR,d_avG2);    
    RK_FFT(d_NZK,d_UK,d_NZR,d_G2R,PlanBatchD2Z,PlanD2Z,d_ikN);
    Step1<<<nblocks,nthreads>>>(d_Z,d_G,d_Z0,d_G0,d_Z2,d_G2,d_NZK,d_UK,d_VK,d_kx,d_ky,d_LL);

    KR_FFT_ALL(d_Z0,d_G0,d_UK,d_VK,d_NZR,PlanBatchZ2D,d_ikF);
    SqrReal<<<nblocks,nthreads>>>(d_G2R,d_NZR);  
    avG2 = thrust::reduce(d_G2Rptr,d_G2Rptr+nr,(double) 0.0,thrust::plus<double>())*in;
    (cudaMemcpy(d_avG2,&avG2, sizeof(double), cudaMemcpyHostToDevice));

    convol<<<nblocks,nthreads>>>(d_G2R,d_NZR,d_avG2);    
    RK_FFT(d_NZK,d_UK,d_NZR,d_G2R,PlanBatchD2Z,PlanD2Z,d_ikN);    
    Step2<<<nblocks,nthreads>>>(d_Z,d_G,d_Z0,d_G0,d_Z3,d_G3,d_NZK,d_UK,d_VK,d_kx,d_ky,d_LL);

    KR_FFT_ALL(d_Z0,d_G0,d_UK,d_VK,d_NZR,PlanBatchZ2D,d_ikF);
    SqrReal<<<nblocks,nthreads>>>(d_G2R,d_NZR);    
    avG2 = thrust::reduce(d_G2Rptr,d_G2Rptr+nr,(double) 0.0,thrust::plus<double>())*in;
    (cudaMemcpy(d_avG2,&avG2, sizeof(double), cudaMemcpyHostToDevice));

    convol<<<nblocks,nthreads>>>(d_G2R,d_NZR,d_avG2);    
    RK_FFT(d_NZK,d_UK,d_NZR,d_G2R,PlanBatchD2Z,PlanD2Z,d_ikN);    
    Step<<<nblocks,nthreads>>>(d_Z,d_G,d_Z0,d_G0,d_Z1,d_G1,d_Z2,d_G2,d_Z3,d_G3,d_NZK,d_UK,d_VK,d_kx,d_ky,d_LL);
	 
    tme += delt2; // Increase time

    fflush(stdout);
  }


  // Copy final state off GPU
  //  (cudaMemcpy(Z, d_Z, sizeof(cufftDoubleComplex)*nkt, cudaMemcpyDeviceToHost));

  cudaEventDestroy( cpu1 );
  cudaEventDestroy( cpu2 );
  fclose(stats);
  fclose(points);
  fclose(vort);
  //Free CPU memory
  free(ZR);
  free(GR);

  // Free GPU global memory
  (cudaFree(d_Z));
  (cudaFree(d_Z0));
  (cudaFree(d_Z1));
  (cudaFree(d_Z2));
  (cudaFree(d_Z3));
  (cudaFree(d_G));
  (cudaFree(d_G0));
  (cudaFree(d_G1));
  (cudaFree(d_G2));
  (cudaFree(d_G3));

  (cudaFree(d_UK));
  (cudaFree(d_VK));
  (cudaFree(d_NZK));
  
  (cudaFree(d_G2R));
  (cudaFree(d_NZR));
  
  (cudaFree(d_ikF));
  (cudaFree(d_ikN));
  (cudaFree(d_kx));
  (cudaFree(d_ky));
  (cudaFree(d_LL));
  (cudaFree(d_avG2));
  
  //Destroy fft plans
  (cufftDestroy(PlanD2Z));
  (cufftDestroy(PlanBatchZ2D));
  (cufftDestroy(PlanBatchD2Z));

}
////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int main(int argc, const char** argv) 
{
  double2 *ZK,*GK;
  double *kx,*ky,*LC;
  int *ikf,*ikn;
  double *lambda,*Tstep,*delta;
  cudaEvent_t start, stop;
  float time;

  lambda=(double*)malloc(sizeof(double));
  Tstep=(double*)malloc(sizeof(double));
  delta=(double*)malloc(sizeof(double));
  // read parameters from params.txt
  read_params(lambda,Tstep,delta);
  //given parameters set certain global host variables
  ikty=(2*ny/3+1);
  iktx=(nx/3+1);
  kty=(ny/3);
  nkt = iktx*ikty;
  nx2= nx/2+1;
  n2= nx2*ny;
  nr = nx*ny;
  in = 1.0/double(nr);
  np[0] = ny;
  np[1] = nx; 
  
  // allocate necessary host arrays
  ZK=(double2*)malloc(sizeof(double2)*nkt);
  GK=(double2*)malloc(sizeof(double2)*nkt);
  kx=(double*)malloc(sizeof(double)*nkt);
  ky=(double*)malloc(sizeof(double)*nkt);
  LC=(double*)malloc(sizeof(double)*nkt);
  ikf=(int*)malloc(sizeof(int)*n2);
  ikn=(int*)malloc(sizeof(int)*nkt);

  //time the run
  cudaEventCreate(&start);
  cudaEventCreate(&stop);  
  
  // set inital condition, wave numbers, filtering and padding arrays.
  init(ZK,GK,kx,ky,LC,ikf,ikn,lambda);

  cudaEventRecord( start, 0 );  

  //do the timestepping
  timeStep(ZK,GK,kx,ky,LC,Tstep,delta,lambda,ikf,ikn);;

  // find the eslapsed time
  cudaThreadSynchronize();
  cudaEventRecord( stop, 0 );
  cudaEventSynchronize( stop );
  cudaEventElapsedTime( &time, start, stop );
  printf ("Time for the original system: %f ms\n", time);
  cudaEventDestroy( start );
  cudaEventDestroy( stop );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

