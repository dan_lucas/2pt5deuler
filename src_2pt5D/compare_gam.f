      program read
C
C READS IN BINARY PHYSICAL DATA AND WRITES FORMATTED FRAME FOR 
C VISUALISATION
C
      IMPLICIT NONE
      INTEGER, PARAMETER  :: rk = 8      ! floating point precision      
      INTEGER, PARAMETER ::NALL=4096
      INTEGER :: ikx,iky,nt,nw,NX,NY
      CHARACTER*40 dfile1,dfile2
C ----------------------------------------------------------------
      REAL(rk) :: ZR1(NALL,NALL),GR1(NALL,NALL),time1,err1
      REAL(rk) :: ZR2(NALL,NALL),GR2(NALL,NALL),time2,err2,err3

c
      print*,'What first data file? (default vort.dat)'
      read(5,'(A)') dfile1 
      if (dfile1(1:1) .eq. ' ') dfile1= 'vort.dat'      
      print*,'What other data file? (default vort.dat)'
      read(5,'(A)') dfile2 
      if (dfile2(1:1) .eq. ' ') dfile2= 'vort.dat'      

      print*,'What resolution (NX,NY)?'
      read(5,*) NX,NY
      print*,'Which output?'
      read(5,*) nw
      print*,'You have selected number ',nw
c
      open(11,file=dfile1,access="STREAM",form='UNFORMATTED')
      open(12,file=dfile2,access="STREAM",form='UNFORMATTED')
      open(13,file='vort_a.dat',form='FORMATTED')
      open(14,file='gamma_a.dat',form='FORMATTED')
c
c vort.dat is the model output file, vort_a.dat is the rewritten ascii version.
c
      do nt=1,10000
         read(11) time1,((ZR1(ikx,iky),ikx=1,NX),iky=1,NY),
     .        ((GR1(ikx,iky),ikx=1,NX),iky=1,NY)
         read(12) time2,((ZR2(ikx,iky),ikx=1,NX),iky=1,NY),
     .        ((GR2(ikx,iky),ikx=1,NX),iky=1,NY)
         if (nt.eq.nw) then
            err1=0._rk
            err2=0._rk
            err3=0._rk
            do iky=1,NY
               write(13,333) (ZR1(ikx,iky)-ZR2(ikx,iky),ikx=1,NX)
               write(13,*) '           '
               write(14,333) (GR1(ikx,iky)**2-GR2(ikx,iky)**2,ikx=1,NX)
               write(14,*) '           '
               do ikx=1,NX
                  err1 = err1+GR1(ikx,iky)**2-GR2(ikx,iky)**2
                  err2 = err2+GR1(ikx,iky)**2
                  err3 = err3+GR2(ikx,iky)**2
               enddo
            enddo
            print*,'time1 = ',time1
            print*,'time2 = ',time2
            print*,'err1 = ',err1
            print*,'err2 = ',err2/(NX*NY)-0.75, err2/(NX*NY)
            print*,'err3 = ',err3/(NX*NY)-0.75, err3/(NX*NY)
            stop
         endif
      enddo
C     
 999  stop
 333  format(1x,E12.5,1x)
      END
C     
