//////////////////////////////////////////////////////////////////////////////////////////////// 
/*
  THIS IS THE MAIN FILE FOR THE 
  **********  MAPPED  *********** 
  PSEUDOSPECTRAL 2.5D EULER MODEL FROM
  MULUNGYE, LUCAS & BUSTAMANTE (2014) WRITTEN IN CUDA FOR USE ON NVIDIA GPU CARDS
  CODE WRITTEN BY DAN LUCAS SEPTEMBER 2014 AT UCD, DUBLIN.

  PSEUDOSPECTRAL STRUCTURE BORROWED FROM NAVIER-STOKES SOLVER WRITTEN BY THE SAME AUTHOR
  USED IN LUCAS & KERSWELL (JFM 2014).

  CURRENT VERSION VALIDATED FOR CUDA 5.5
  DEPENDENCIES AS BELOW AND IN MAKEFILE. SEE README FOR MORE INFO.
*/
//////////////////////////////////////////////////////////////////////////////////////////////// 
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <helper_cuda.h>
#include <cufft.h>
#include <cuda_runtime.h>
#include <thrust/functional.h>
#include <thrust/transform_reduce.h>
#include <thrust/reduce.h>
#include <thrust/extrema.h>
#include <thrust/device_vector.h>
#include <gsl/gsl_sf_lambert.h>
#include "kernels.h"
#include "functions.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MAIN TIMESTEPPING ROUTINE                                  
void timeStep(double2 *Z,
	      double2 *G,
	      double *kx,
	      double *ky,
	      double *LC,
	      double *Tstep,
	      double *delta,
	      double *lambda,
	      int *ikF,
	      int *ikN)
{  
  printf("\n In timestep \n");

  // open output files, named with resolution and for mapped version
  char statsfile[sizeof "map_0000.dat"];
  sprintf(statsfile, "map_%04d.dat",nx);
  char specfile[sizeof "spec_map_0000.dat"];
  sprintf(specfile, "spec_map_%04d.dat",nx);
  char ensfile[sizeof "vort_map_0000.dat"];
  sprintf(ensfile, "vort_map_%04d.dat",nx);

  vort = fopen(ensfile,"wb");
  points = fopen(specfile,"w");
  stats = fopen(statsfile,"w");

  int n0=15000;
  int it=0;
  double *tau0,*Gam0;
  // For the case lambda=0 read in the accompanying data files
  // which contain tau and gamma_SUP from S ODE for comparison
  // to full PDE integration.
  if(*lambda==0.0){
     tau0=(double*)malloc(sizeof(double)*n0);
     Gam0=(double*)malloc(sizeof(double)*n0);
     FILE * gfile=fopen("gamma_tau_lambda_0_fine3_tau_15.dat","r");
     double dum1,dum2,anaG;
     for(int i=0; i<n0; i++){
       fscanf(gfile," %le %le\n",&dum1,&dum2);
       double aZ = -0.0625*twopi*exp(-dum1);
       if(aZ > -1.0/exp(1.0)){
	 double ZZ = -gsl_sf_lambert_Wm1(aZ);
	 //	 anaG = twopi*twopi*exp(ZZ)*(ZZ-1.0)/(ZZ*ZZ*ZZ*32.0);
       }
       //       printf(" %le %le %le %le\n",dum1,dum2,anaG,abs(anaG/dum2-1.0));
       tau0[i]=dum1;
       Gam0[i]=dum2;
     }
     fclose(gfile);
  }else if(*lambda != -1.5){
     printf("\n ******** WARNING: \n YOUR CHOICE OF LAMBDA HAS NO EXACT SOLUTION CODED FOR COMPARISON TO NUMERICAL SOLUTION *****************  \n (errors output will be meaningless!)");
   }

  // define some host variables
  double *ZR,*GR;
  double Gmax,Gmax0,Zmax,xgmax,ygmax;
  double maxG,avg1,avg2,avg3,avGint,anaG,anaZ,Tstar,gradGsq;
  double Ga2,Gn2,Ge2, Oa2,On2,Oe2,maxG1,maxG2,errT;
  size_t avail,total;
  float time;
  cudaEvent_t cpu1, cpu2;
  int NSTOP = int(*Tstep/(*delta));
  ZR=(double*)malloc(sizeof(double)*nr);
  GR=(double*)malloc(sizeof(double)*nr);
  cudaEventCreate(&cpu1);
  cudaEventCreate(&cpu2);
  // Define global device variables
  int *d_ikF, *d_ikN;
  double *d_avG2,*d_LL;
  cufftDoubleComplex *d_Z,*d_Z0,*d_Z1,*d_Z2,*d_Z3;
  cufftDoubleComplex *d_G,*d_G0,*d_G1,*d_G2,*d_G3;
  cufftDoubleComplex  *d_UK, *d_VK,*d_NZK;
  cufftDoubleReal *d_G2R,*d_NZR;
  cufftHandle PlanBatchZ2D,PlanBatchD2Z,PlanD2Z;

  // Allocate global memory on GPU. (Constant memory does not need allocating) 	
  (cudaMalloc((void**)&d_Z,sizeof(cufftDoubleComplex)*nkt));
  (cudaMalloc((void**)&d_Z0,sizeof(cufftDoubleComplex)*nkt));
  (cudaMalloc((void**)&d_Z1,sizeof(cufftDoubleComplex)*nkt));
  (cudaMalloc((void**)&d_Z2,sizeof(cufftDoubleComplex)*nkt));
  (cudaMalloc((void**)&d_Z3,sizeof(cufftDoubleComplex)*nkt));

  (cudaMalloc((void**)&d_G,sizeof(cufftDoubleComplex)*nkt));
  (cudaMalloc((void**)&d_G0,sizeof(cufftDoubleComplex)*nkt));
  (cudaMalloc((void**)&d_G1,sizeof(cufftDoubleComplex)*nkt));
  (cudaMalloc((void**)&d_G2,sizeof(cufftDoubleComplex)*nkt));
  (cudaMalloc((void**)&d_G3,sizeof(cufftDoubleComplex)*nkt));

  (cudaMalloc((void**)&d_UK,sizeof(cufftDoubleComplex)*nkt));
  (cudaMalloc((void**)&d_VK,sizeof(cufftDoubleComplex)*nkt));

  (cudaMalloc((void**)&d_NZK,sizeof(cufftDoubleComplex)*4*nkt));
  
  (cudaMalloc((void**)&d_NZR,sizeof(cufftDoubleReal)*4*nr));
  (cudaMalloc((void**)&d_G2R,sizeof(cufftDoubleReal)*nr));
  
  (cudaMalloc((void**)&d_kx,sizeof(double)*nkt));
  (cudaMalloc((void**)&d_ky,sizeof(double)*nkt));
  (cudaMalloc((void**)&d_LL,sizeof(double)*nkt));
  (cudaMalloc((void**)&d_ikF,sizeof(int)*n2));
  (cudaMalloc((void**)&d_ikN,sizeof(int)*nkt));
  (cudaMalloc((void**)&d_avG2,sizeof(double)));
  
  // Copy state data to GPU global memory 
  (cudaMemcpy(d_Z,Z,sizeof(cufftDoubleComplex)*nkt,cudaMemcpyHostToDevice));
  (cudaMemcpy(d_G,G,sizeof(cufftDoubleComplex)*nkt,cudaMemcpyHostToDevice));
  
  // Copy constant parameters to GPU constant memory
  (cudaMemcpyToSymbol(d_DELTA,delta,sizeof(double)));
  (cudaMemcpyToSymbol(d_LAM,lambda,sizeof(double)));
  (cudaMemcpyToSymbol(d_IN,&in,sizeof(double)));
  (cudaMemcpyToSymbol(d_IKTX,&iktx,sizeof(int)));
  (cudaMemcpyToSymbol(d_IKTY,&ikty,sizeof(int)));
  (cudaMemcpyToSymbol(d_KTY,&kty,sizeof(int)));
  (cudaMemcpyToSymbol(d_NX,&nx,sizeof(int)));
  (cudaMemcpyToSymbol(d_NY,&ny,sizeof(int)));
  (cudaMemcpyToSymbol(d_OR,&nr,sizeof(int)));
  (cudaMemcpyToSymbol(d_OK,&nkt,sizeof(int)));
  (cudaMemcpyToSymbol(d_O2,&n2,sizeof(int)));

  //Set up various arrays to enable generic kernel calls
  //i.e. calculate indexing for padding either side of FFTs, wavenumber arrays, mask, and timestep arrays.
  // This must be done on CPU for scalability (large problems violate max threads per block)
  (cudaMemcpy(d_kx,kx,sizeof(double)*nkt,cudaMemcpyHostToDevice));
  (cudaMemcpy(d_ky,ky,sizeof(double)*nkt,cudaMemcpyHostToDevice));
  (cudaMemcpy(d_LL,LC,sizeof(double)*nkt,cudaMemcpyHostToDevice));
  (cudaMemcpy(d_ikF,ikF,sizeof(int)*n2,cudaMemcpyHostToDevice));
  (cudaMemcpy(d_ikN,ikN,sizeof(int)*nkt,cudaMemcpyHostToDevice));
  //Set FFT Plans balance FFT work, batch 4 FFTs in each direction
  // plus one extra for gamma^2
  (cufftPlan2d(&PlanD2Z,ny,nx,CUFFT_D2Z));
  (cufftPlanMany(&PlanBatchD2Z,2,np,NULL,1,0,NULL,1,0,CUFFT_D2Z,4));
  (cufftPlanMany(&PlanBatchZ2D,2,np,NULL,1,0,NULL,1,0,CUFFT_Z2D,4));
  // Cast thrust pointers for computing average and finding maximum
  thrust::device_ptr<double> d_NZRptr(d_NZR);
  thrust::device_ptr<double> d_G2Rptr(d_G2R);

  // Do a check of global memory use
  avail =0;
  total = 0;
  cudaMemGetInfo(&avail,&total);
  
  printf("\n total : %f MB \n",double(total)/(1024.0f*1024.0f));
  printf("\n avail : %f MB \n",double(avail)/(1024.0f*1024.0f));
  printf("\n used : %f MB \n",double(total-avail)/(1024.0f*1024.0f));

  // Do an initial set of velocity coeffs. Subsequently this occurs at the end of stepping kernels
  setVelocity<<<nblocks,nthreads>>>(d_Z,d_G,d_Z0,d_G0,d_UK,d_VK,d_kx,d_ky,d_LL);

  // intialise cumulative quantities
  avg1=0.0;
  avg2=0.0;
  avg3=0.0;
  avGint=0.0;
  Tstar=0.0;
  tme=0.0;
  Ga2=0.0;
  Gn2=0.0;
  Ge2=0.0;
  Oa2=0.0;
  On2=0.0;
  Oe2=0.0;
  // **************************
  //STEPPING STARTS HERE
  // **************************
  cudaEventRecord( cpu1, 0 );

  for(int NT=0; NT<NSTOP; NT++){

    KR_FFT_ALL(d_Z0,d_G0,d_UK,d_VK,d_NZR,PlanBatchZ2D,d_ikF); // spec-phys transform
    // make G2R absolute value of G for getting max (thrust max element doesn't do max(abs(G))
    absReal<<<nblocks,nthreads>>>(d_G2R,d_NZR);    
    get_max(d_NZR,d_G2Rptr,Gmax,xgmax,ygmax); //find the max of |gamma|
    if(NT==0)Gmax0=abs(Gmax); //set initial max(gamma)
    (cudaMemcpyToSymbol(d_MAX,&Gmax,sizeof(double))); //copy max to device
    normalise<<<nblocks,nthreads>>>(d_NZR); //normalise all phys variables with max(gamma)
    normaliseK<<<nblocks,nthreads>>>(d_Z,d_Z0,d_G,d_G0);// also need to normalise the first RK4 array
    SqrReal<<<nblocks,nthreads>>>(d_G2R,d_NZR);    // Do real space square (needed for average)
    cudaThreadSynchronize();
    avg3 = (thrust::reduce(d_G2Rptr,d_G2Rptr+nr,(double) 0.0,thrust::plus<double>()))*in;// average the square
    (cudaMemcpy(d_avG2,&avg3, sizeof(double), cudaMemcpyHostToDevice)); // copy <g^2> to device

    double tcrit=TOut*double(int((tme+small)/TOut));//want outputs every Tout!
    if((tme-*delta-tcrit+small)*(tme+small-tcrit) < 0.0){//sporadic outputs of physical field and spectrum of gamma
	(cudaMemcpy(G,d_G,sizeof(cufftDoubleComplex)*nkt,cudaMemcpyDeviceToHost));
// 	(cudaMemcpy(ZR,d_NZR+nr,sizeof(cufftDoubleReal)*nr,cudaMemcpyDeviceToHost));
// 	(cudaMemcpy(GR,d_NZR,sizeof(cufftDoubleReal)*nr,cudaMemcpyDeviceToHost));
// 	fwrite(&tme,sizeof(double),1,vort);
// 	fwrite(ZR,sizeof(cufftDoubleReal),nr,vort);
// 	fwrite(GR,sizeof(cufftDoubleReal),nr,vort);
// 	fflush(vort);

	double spec[ikty];
	gradGsq=0;
	int size = 0;
	for(int i=0; i<ikty; i++) spec[i]=0.0;
	for(int i=0; i<nkt; i++){
	  if(LC[i] != 0.0 ){
	    double wk = sqrt(max(kx[i]*kx[i] + ky[i]*ky[i],0.001));
	    double VZ = G[i].x*G[i].x + G[i].y*G[i].y;
	    int j = int(wk+0.5)-1;
	    spec[j] += VZ;
	    size = max(size,j);
	    gradGsq += wk*wk*VZ;
	  }
	}

	for(int j=0; j<size; j++){
	  fprintf(points,"%d %e \n",j,spec[j]);
	}
	fprintf(points,"  \n");
	fflush(points);
    }
    // find omega at position of max gamma
    interpolate_point(d_NZR,Zmax,xgmax,ygmax);
    if(NT==0)Gmax=-Gmax;

    avg3 = sgn(Gmax)*((2+*lambda)*avg3-(1+*lambda));
    if(NT==0){
      Tstar = 0.0;
    }else{
      avGint += *delta*avg3; //compute integral <g^2> 
      //      Tstar += *delta/maxG;
    }
    
    maxG = abs(Gmax0)*exp(avGint); // evaluate max(gamma)(tau)
    if(NT==0){
      Tstar = 0.0;
    }else{
      if(NT % 2 ==0){
	Tstar += *delta*third*(1.0/maxG1 + 4.0/maxG2 + 1.0/maxG);
      }
    }
    //    maxG = abs(Gmax0)*exp(-(1.0+*lambda)*tme+(2.0+*lambda)*avGint); // evaluate max(gamma)(tau)

    cudaThreadSynchronize();
    cudaEventRecord( cpu2, 0 );
    cudaEventSynchronize( cpu2 );
    cudaEventElapsedTime( &time, cpu1, cpu2);
    // get analytic (or S eqn derived) expressions for computing error
    // only coded for lambda=-3/2 and lamba=0 cases, be careful of output for other cases
    if(*lambda==-1.5){
      anaG = 0.5*exp(0.5*tme)*sqrt(11.0-3.0*exp(-tme));// analytic gamma(tau)    
      anaZ = exp(tme);
//     }else if(*lambda==0.0){
//       if(tme==0.0){
// 	anaG=1.0;
//       }else{
// 	while(tau0[it]<tme){
// 	  it++;
// 	}
// 	//linear interpolation to get "analytic" gamma.
// 	anaG=(Gam0[it]*(tme-tau0[it-1])+Gam0[it-1]*(tau0[it]-tme))/(tau0[it]-tau0[it-1]);
//       }
//       anaZ=1.0; // don't have omega...                                                                                           

    double errZ = abs(anaZ/abs(Zmax*maxG)-1.0); //error in omega
    double errG = abs(maxG/anaG - 1.0); // error in gamma
    Ga2 +=anaG*anaG;
    Gn2 +=maxG*maxG;
    Ge2 +=pow(maxG-anaG,2);
    Oa2 +=anaZ*anaZ;
    On2 +=Zmax*Zmax*maxG*maxG;
    Oe2 +=pow(abs(Zmax*maxG)-anaZ,2);
    //    errT = abs(Tstar/(atan(root2*Cfac)/Cfac)-1.0); //error in Tstar
    fprintf(stats,"%e %e %e %e %e %e %e %e %.15e\n",tme,time,maxG,Zmax,errG,gradGsq,Ge2/(Ga2+Gn2),Oe2/(Oa2+On2),Tstar);

    }else{
      double aZ = -0.0625*twopi*exp(-tme);
      if(aZ > -1.0/exp(1.0)){
	double ZZ = -gsl_sf_lambert_Wm1(aZ);
	anaG = twopi*twopi*exp(ZZ)*(ZZ-1.0)/(ZZ*ZZ*ZZ*32.0);
      }else{
	anaG=1.0;
      }
      double errG = abs(maxG/anaG - 1.0); // error in gamma
      double errG2 = abs(maxG/Gam0[NT] - 1.0); // error in gamma
      //      errT = abs(Tstar/1.418002734923858875 - 1.0); //error in Tstar
      fprintf(stats,"%.15e %.15e %e %e %e %e %e %e %.15e\n",tme,maxG,Zmax,time,errT,gradGsq,anaG,errG,errG2,Tstar);
    }
    avg1=avg2;
    avg2=avg3;
    maxG1=maxG2;
    maxG2=maxG;
    fflush(stats);

    convol<<<nblocks,nthreads>>>(d_G2R,d_NZR,d_avG2);    // Do real space convolution, both terms inside NZR	 
    RK_FFT(d_NZK,d_UK,d_NZR,d_G2R,PlanBatchD2Z,PlanD2Z,d_ikN);    // RK does a batch of 4+1 ffts
    Step1<<<nblocks,nthreads>>>(d_Z,d_G,d_Z0,d_G0,d_Z1,d_G1,d_NZK,d_UK,d_VK,d_kx,d_ky,d_LL,d_avG2); //first mid step
    // structure repeats now:

    KR_FFT_ALL(d_Z0,d_G0,d_UK,d_VK,d_NZR,PlanBatchZ2D,d_ikF);
    absReal<<<nblocks,nthreads>>>(d_G2R,d_NZR);    
    get_max(d_NZR,d_G2Rptr,Gmax,xgmax,ygmax);

    (cudaMemcpyToSymbol(d_MAX,&Gmax,sizeof(double)));
    normalise<<<nblocks,nthreads>>>(d_NZR);
    SqrReal<<<nblocks,nthreads>>>(d_G2R,d_NZR);
    cudaThreadSynchronize();

    avg3 = thrust::reduce(d_G2Rptr,d_G2Rptr+nr,(double) 0.0,thrust::plus<double>())*in;
    (cudaMemcpy(d_avG2,&avg3, sizeof(double), cudaMemcpyHostToDevice));

    convol<<<nblocks,nthreads>>>(d_G2R,d_NZR,d_avG2);  
    RK_FFT(d_NZK,d_UK,d_NZR,d_G2R,PlanBatchD2Z,PlanD2Z,d_ikN);   
    Step1<<<nblocks,nthreads>>>(d_Z,d_G,d_Z0,d_G0,d_Z2,d_G2,d_NZK,d_UK,d_VK,d_kx,d_ky,d_LL,d_avG2);

    KR_FFT_ALL(d_Z0,d_G0,d_UK,d_VK,d_NZR,PlanBatchZ2D,d_ikF);
    absReal<<<nblocks,nthreads>>>(d_G2R,d_NZR);    
    get_max(d_NZR,d_G2Rptr,Gmax,xgmax,ygmax);
    (cudaMemcpyToSymbol(d_MAX,&Gmax,sizeof(double)));
    normalise<<<nblocks,nthreads>>>(d_NZR);
    SqrReal<<<nblocks,nthreads>>>(d_G2R,d_NZR);    

    cudaThreadSynchronize();
    avg3 = thrust::reduce(d_G2Rptr,d_G2Rptr+nr,(double) 0.0,thrust::plus<double>())*in;
    (cudaMemcpy(d_avG2,&avg3, sizeof(double), cudaMemcpyHostToDevice));

    convol<<<nblocks,nthreads>>>(d_G2R,d_NZR,d_avG2);  
    RK_FFT(d_NZK,d_UK,d_NZR,d_G2R,PlanBatchD2Z,PlanD2Z,d_ikN); 
    Step2<<<nblocks,nthreads>>>(d_Z,d_G,d_Z0,d_G0,d_Z3,d_G3,d_NZK,d_UK,d_VK,d_kx,d_ky,d_LL,d_avG2);

    KR_FFT_ALL(d_Z0,d_G0,d_UK,d_VK,d_NZR,PlanBatchZ2D,d_ikF);
    absReal<<<nblocks,nthreads>>>(d_G2R,d_NZR);   
    get_max(d_NZR,d_G2Rptr,Gmax,xgmax,ygmax);
    (cudaMemcpyToSymbol(d_MAX,&Gmax,sizeof(double)));
    normalise<<<nblocks,nthreads>>>(d_NZR);
    SqrReal<<<nblocks,nthreads>>>(d_G2R,d_NZR);   

    cudaThreadSynchronize();
    avg3 = thrust::reduce(d_G2Rptr,d_G2Rptr+nr,(double) 0.0,thrust::plus<double>())*in;
    (cudaMemcpy(d_avG2,&avg3, sizeof(double), cudaMemcpyHostToDevice));

    convol<<<nblocks,nthreads>>>(d_G2R,d_NZR,d_avG2);  
    RK_FFT(d_NZK,d_UK,d_NZR,d_G2R,PlanBatchD2Z,PlanD2Z,d_ikN);    
    Step<<<nblocks,nthreads>>>(d_Z,d_G,d_Z0,d_G0,d_Z1,d_G1,d_Z2,d_G2,d_Z3,d_G3,d_NZK,d_UK,d_VK,d_kx,d_ky,d_LL,d_avG2);
	 
    tme = *delta*(NT+1); // Increase time

    fflush(stdout);
  }


  // Copy final state off GPU (maybe)
  (cudaMemcpy(G, d_G, sizeof(cufftDoubleComplex)*nkt, cudaMemcpyDeviceToHost));
  //output final spectrum
  double spec[ikty];
  int size = 0;
  for(int i=0; i<ikty; i++) spec[i]=0.0;
  for(int i=0; i<nkt; i++){
    if(LC[i] != 0.0 ){
      double wk = sqrt(max(kx[i]*kx[i] + ky[i]*ky[i],0.001));
      double VZ = G[i].x*G[i].x + G[i].y*G[i].y;
      int j = int(wk+0.5)-1;
      spec[j] += VZ;
      size = max(size,j);
    }
  }
  
  for(int j=0; j<size; j++){
    fprintf(points,"%d %e \n",j,spec[j]);
  }
  fprintf(points,"  \n");
  fflush(points);

  cudaEventDestroy( cpu1 );
  cudaEventDestroy( cpu2 );

  fclose(stats);
  fclose(points);
  fclose(vort);
  //Free CPU memory
  free(ZR);
  free(GR);

  // Free GPU global memory
  (cudaFree(d_Z));
  (cudaFree(d_Z0));
  (cudaFree(d_Z1));
  (cudaFree(d_Z2));
  (cudaFree(d_Z3));
  (cudaFree(d_G));
  (cudaFree(d_G0));
  (cudaFree(d_G1));
  (cudaFree(d_G2));
  (cudaFree(d_G3));

  (cudaFree(d_UK));
  (cudaFree(d_VK));
  (cudaFree(d_NZK));
  
  (cudaFree(d_G2R));
  (cudaFree(d_NZR));
  
  (cudaFree(d_ikF));
  (cudaFree(d_ikN));
  (cudaFree(d_kx));
  (cudaFree(d_ky));
  (cudaFree(d_LL));
  (cudaFree(d_avG2));
  
  //Destroy fft plans
  (cufftDestroy(PlanD2Z));
  (cufftDestroy(PlanBatchZ2D));
  (cufftDestroy(PlanBatchD2Z));

}
////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int main(int argc, const char** argv) 
{

  double2 *ZK,*GK;
  double *kx,*ky,*LC;
  int *ikf,*ikn;
  double *lambda,*Tstep,*delta;
  cudaEvent_t start, stop;
  float time;

  lambda=(double*)malloc(sizeof(double));
  Tstep=(double*)malloc(sizeof(double));
  delta=(double*)malloc(sizeof(double));
  read_params(lambda,Tstep,delta);
  ikty=(2*ny/2+1);
  iktx=(nx/2+1);
  kty=(ny/2);
  nkt = iktx*ikty;
  nx2= nx/2+1;
  n2= nx2*ny;
  nr = nx*ny;
  in = 1.0/double(nr);
  np[0] = ny;
  np[1] = nx; 
  
  ZK=(double2*)malloc(sizeof(double2)*nkt);
  GK=(double2*)malloc(sizeof(double2)*nkt);
  kx=(double*)malloc(sizeof(double)*nkt);
  ky=(double*)malloc(sizeof(double)*nkt);
  LC=(double*)malloc(sizeof(double)*nkt);
  ikf=(int*)malloc(sizeof(int)*n2);
  ikn=(int*)malloc(sizeof(int)*nkt);
  
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  
  init(ZK,GK,kx,ky,LC,ikf,ikn,lambda);

  cudaEventRecord( start, 0 );

  timeStep(ZK,GK,kx,ky,LC,Tstep,delta,lambda,ikf,ikn);;

  cudaThreadSynchronize();
  cudaEventRecord( stop, 0 );
  cudaEventSynchronize( stop );
  cudaEventElapsedTime( &time, start, stop );
  printf ("Time for the mapped system: %f ms\n", time);
  cudaEventDestroy( start );
  cudaEventDestroy( stop );

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

