#!/usr/bin/python

import numpy as np
import math

nk=input("how big is each spectrum? ")
ns=input("which spectrum would you like? ")
filein=raw_input("which file? ")
fileout=open('spec1.dat','w')
data = np.loadtxt(filein)

for k in range((nk+1)*ns,(nk+1)*(ns+1)):
    b=data[k][:].reshape((1,2))
    np.savetxt(fileout,b,'%1.5e %1.5e')
        
