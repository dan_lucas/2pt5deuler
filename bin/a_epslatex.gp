set terminal epslatex color size 6cm,6cm
set output "fig.tex"
set size ratio 1.0
set lmargin at screen 0.0
set bmargin at screen 0.1
set tmargin at screen 0.99
set rmargin at screen 0.99
set pm3d map
#set contour
set xrange [0:511]
set yrange [0:511]
unset xtics
unset ytics
set xtics ('$0$' 0,'\pi$' 256,'2$\pi$' 511)
set ytics ('$0$' 0,'\pi$' 256,'2$\pi$' 511)
#set ytics ('$0$' 0,'$2\pi$' 127)
set cbrange [-1:1]
set palette rgb 21,22,23;
#set palette defined (-3  "blue", 0 "white", 3 "red")
unset colorbox
set multiplot 
splot 'vort_a.dat' with pm3d notitle
set label "${t=0}$" at screen 0,-0.01 front
set contour
set cntrparam level incremental -1, 4, 1
unset surface
unset key
unset clabel
splot 'vort_a.dat' with lines notitle "$t=0$" lc rgb "#000000" lw 0.5
unset multiplot