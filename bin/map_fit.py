#!/usr/bin/python

import numpy as np
import math

lam=-1.5
T=1000
dt=0.01
data = np.loadtxt("map_1024.dat")

Texact=(4.0/np.sqrt(3))*np.arctan(np.sqrt(6)/4.0)
for tstep in range(20,150):
    trel=tstep/10.0
    temp_f=[]
    temp_t=[]
    for k in range(len(data)):
        temp_f.append(np.log(data[k][3]))
        temp_t.append(data[k][0])
        if (data[k][0]) >= trel :
            gmax=1.0/data[k][2]
            Tstrel=data[k][8]
            break

    regress=np.polyfit(temp_t,temp_f,1)

#    nst = len(temp_t)-1
    n=regress[1]
    alpha= np.exp(regress[0])
    alphan= np.exp(regress[0])/n
    etrel=np.exp(-trel)

    NT=int((T-trel)/dt)
    intg=0.0
    for i in range(NT/2-2):
        t1=trel+(2*i)*dt
        t2=trel+(2*i+1)*dt
        t3=trel+(2*i+2)*dt
        int1 = np.exp((1+lam)*(t1-trel)-(2+lam)*alphan*(np.exp(n*t1)-np.exp(n*trel)))
        int2 = np.exp((1+lam)*(t2-trel)-(2+lam)*alphan*(np.exp(n*t2)-np.exp(n*trel)))
        int3 = np.exp((1+lam)*(t3-trel)-(2+lam)*alphan*(np.exp(n*t3)-np.exp(n*trel)))
        intg += (dt/3.0)*(int1+4.0*int2+int3)

    Tstar1 = Tstrel +gmax*intg

    intg=0.0
    for i in range(NT):
        t1=trel+(i)*dt
        int1 = np.exp((1+lam)*(t1-trel)-(2+lam)*alphan*(np.exp(n*t1)-np.exp(n*trel)))
        intg += (dt)*(int1)

    Tstar2 = Tstrel +gmax*intg

#    A = (1.0+lam)*T + alphan*(2.0+lam)*(np.exp(n*trel)-np.exp(n*T))+trel*(etrel-lam)-T*etrel
#    B = (1.0+lam)*T + alphan*(2.0+lam)*(np.exp(n*trel)-np.exp(n*T))+trel*(etrel-lam)

#    Tstar= Tstrel - gmax*np.exp(-etrel*trel)*(np.exp(trel)-np.exp(trel+etrel*trel)-np.exp(A)+np.exp(B))

    print trel,Tstar1,Tstar1-Texact,Tstar2,Tstar2-Texact
# for k in range(nst,len(data)):
#     exact = data[nst][2]*np.exp(0.5*(data[k][0]-data[nst][0])+0.5*(alpha/n)*(np.exp(n*data[k][0])-np.exp(n*data[nst][0])))
#     print data[k][0],exact
#        
