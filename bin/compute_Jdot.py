#!/usr/bin/python

import numpy as np
import math

filein=raw_input("which file? ")
fileout=open('Jdot.dat','w')
data = np.loadtxt(filein)

for k in range(1,len(data)-1):
    Jold = abs(data[k-1][2]*data[k-1][5]-data[k-1][3]*data[k-1][4])
    J = abs(data[k+1][2]*data[k+1][5]-data[k+1][3]*data[k+1][4])
    Jdot = (Jold-J)/(data[k+1][0]-data[k-1][0])
    Jnow = abs(data[k][2]*data[k][5]-data[k][3]*data[k][4])
    tnow = data[k][0]
    b=np.array([tnow,Jdot,Jnow]).reshape((1,3))    
    np.savetxt(fileout,b,'%1.5e %1.8e %1.8e')
