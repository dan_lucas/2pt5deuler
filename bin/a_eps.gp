set terminal postscript enhanced color eps
set output "spec_plot.eps"
set xrange [0:512]
set yrange [-512:512]
set pm3d map
set size ratio 2
set cbrange [1E-16:1]
set logscale cb
set xlabel 'kx'
set ylabel 'ky'
splot 'spec_a.dat' u 1:2:3 notitle