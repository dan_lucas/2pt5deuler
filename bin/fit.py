#!/usr/bin/python

import numpy as np
import math

Dt=0.05
data = np.loadtxt("stats_0512.dat")

for k in range(len(data)-1,0,-1):
    temp_f=[]
    temp_t=[]
    for i in range(k,0,-1):
        temp_f.append(data[i][4]/data[i][7])
        temp_t.append(data[i][1])
        if (data[k][1]-data[i][1]) > Dt :
            regress=np.polyfit(temp_t,temp_f,1)
            j=len(temp_t)-1
            alpha = (temp_f[0]-temp_f[j])/(temp_t[0]-temp_t[j])
            Tstar = -(temp_f[0]/alpha - temp_t[0])

            print data[k][0],1.0/regress[0],-regress[1]/regress[0],1.0/alpha,Tstar
            break
        
