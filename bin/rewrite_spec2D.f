      program read
      IMPLICIT NONE
      INTEGER NX,NY,ikx,iky,nt,nw,ns,KTX,KTY,IKTX,IKTY
      REAL alpha
C -----------------------------------------------------------------                                                                     
      PARAMETER(NY  =512)

      REAL VZ_xy,VZ_x,VZ_y,kx,ky
c                                                                                                                                       
      NT=(NY*2+1)*(NY+1)
      print*,'Which output?'
      read(5,*) nw
      print*,'You have selected number ',nw
c                                                                                                                                       
      open(32,file='spec2D_map_1024.dat',form='FORMATTED')
      open(34,file='spec_a.dat',form='FORMATTED')
c                                                                                                                                       
c vort.dat is the model output file, vort_a.dat is the rewritten ascii version.                                                         
c                                                                                                                                       
      do nt=1,1000
         do iky=1,NY*2+1
            do ikx=1,NY+1
               read(32,*) kx,ky,VZ_x
               if (nt.eq.nw) then
                  write(34,333) kx,ky,VZ_x
               endif
            enddo
            if (nt.eq.nw) write(34,*) '   '
         enddo

         if (nt.eq.nw) then
            print*,'time = ',nt
            stop
         endif
c          print*,'nt=',nt                                                                                                              
      enddo
C                                                                                                                                       
 999     stop
 333       format(4(E12.5,1x))
      END
C           
