// Some compiler macro constants (host and/or device)
#define sixth (1.0/6.0)
#define third (1.0/3.0)
#define Cfac (sqrt(3.0)/4.0)
#define root2 (sqrt(2.0))
#define nthreads 512
#define nblocks 140
#define small 1e-8
// Threads should be a multiple of warp size (32) and maximise warps per multiproc to hide register latency (>192)
// Blocks should be a multiple of multiprocs (14) and >100 for scalability
// Check your own system and make appropriate adjustments to nthread and nblocks!
////////////////////////////////////////////////////////////////////////
// Define constant device variables
////////////////////////////////////////////////////////////////////////
__constant__ int d_IKTX, d_IKTY, d_KTY, d_NX, d_NY,d_KFX,d_KFY,d_OR,d_OK,d_O2;
__constant__ double d_IN,d_LAM,d_DELTA,d_MAX,d_NU;

// Define constant host variables
////////////////////////////////////////////////////////////////////////

int nkt,iktx,ikty,kty,nr,nx,ny,nx2,n2;
int np[2];
double in,freq2,tme,tstart,delt,energy,TOut,VOut,SOut,Nu;
const double twopi = 4.0*asin(1.0);

// global device pointers
double *d_kx,*d_ky;
FILE * stats;
FILE * spec2D;
FILE * specxy;
FILE * specsh;
FILE * errf;
FILE * vort;

//sign of double                                                                                                                     
__host__ __device__ int sgn(double d){ return ((d > 0.) - (0. > d));}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//******************  GPU KERNELS    ***************************
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void setFFN(cufftDoubleComplex *V1,cufftDoubleComplex *V2,cufftDoubleComplex *V3,cufftDoubleComplex *V4, cufftDoubleComplex *F1, int *ikF){
  int ik = threadIdx.x + blockIdx.x*blockDim.x;

  while(ik < d_O2){
    // Map -Ky values to -Ky+NY and fill trucated wave numbers with zeros
    // Note ikF is set with indexing for this on the fortran side
      int ikk = ikF[ik];
      if(ikk < 0){
      	F1[ik].x = 0.0;
      	F1[ik].y = 0.0;
      	F1[ik+d_O2].x = 0.0;
      	F1[ik+d_O2].y = 0.0;
      	F1[ik+2*d_O2].x = 0.0;
      	F1[ik+2*d_O2].y = 0.0;
      	F1[ik+3*d_O2].x = 0.0;
      	F1[ik+3*d_O2].y = 0.0;
      }else{
	F1[ik].x = V1[ikk].x;
	F1[ik].y = V1[ikk].y;
	F1[ik+d_O2].x = V2[ikk].x;
	F1[ik+d_O2].y = V2[ikk].y;
	F1[ik+2*d_O2].x = V3[ikk].x;
	F1[ik+2*d_O2].y = V3[ikk].y;
	F1[ik+3*d_O2].x = V4[ikk].x;
	F1[ik+3*d_O2].y = V4[ikk].y;
      }
	
    ik+= blockDim.x*gridDim.x;
  }
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////\
///////////

__global__ void conj(cufftDoubleComplex *ZK,cufftDoubleComplex *GK,cufftDoubleComplex *UK,cufftDoubleComplex *VK, double *kx, double *ky){
  // Apply conjugate symmetry condition Z(0,-ky)=conj(Z(0,ky)
  int ik = threadIdx.x + blockIdx.x*blockDim.x;

  while(ik < d_OK){
    if( kx[ik] == 0.0 && ky[ik] < 0.0){
      int iky = ik/d_IKTX;
      int ikk = (d_IKTY-iky-1)*d_IKTX;

      ZK[ik].x =  ZK[ikk].x;
      ZK[ik].y = -ZK[ikk].y;
      GK[ik].x =  GK[ikk].x;
      GK[ik].y = -GK[ikk].y;
      UK[ik].x =  UK[ikk].x;
      UK[ik].y = -UK[ikk].y;
      VK[ik].x =  VK[ikk].x;
      VK[ik].y = -VK[ikk].y;
    }
    ik += blockDim.x*gridDim.x;
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////\
/////////

__global__ void setVelocity(cufftDoubleComplex *ZK,cufftDoubleComplex *GK,cufftDoubleComplex *Z0,cufftDoubleComplex *G0,cufftDoubleComplex *UK,cufftDoubleComplex *VK,double *kx,double *ky){
  // Kernel to set the spectral velocity coefficients UK and VK for first step
  int ik = threadIdx.x + blockIdx.x*blockDim.x;

  while(ik < d_OK){

    double kkx = kx[ik];
    double kky = ky[ik];
    
    double wk =kkx*kkx + kky*kky;
    double wki = 1.0/(max(wk,0.001));
    
    UK[ik].x = -(kky*ZK[ik].y+kkx*GK[ik].y)*wki;
    UK[ik].y =  (kky*ZK[ik].x+kkx*GK[ik].x)*wki;
    
    VK[ik].x =  (kkx*ZK[ik].y-kky*GK[ik].y)*wki;
    VK[ik].y = -(kkx*ZK[ik].x-kky*GK[ik].x)*wki;

    Z0[ik].x=ZK[ik].x;
    Z0[ik].y=ZK[ik].y;
    G0[ik].x=GK[ik].x;
    G0[ik].y=GK[ik].y;

   ik += blockDim.x*gridDim.x;
  }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void derivVelocity(cufftDoubleComplex *ZK,cufftDoubleComplex *GK,cufftDoubleComplex *UX,cufftDoubleComplex *UY,cufftDoubleComplex *VX,cufftDoubleComplex *VY,double *kx,double *ky){
  // Kernel to set the spectral velocity coefficients UK and VK for first step
  int ik = threadIdx.x + blockIdx.x*blockDim.x;

  while(ik < d_OK){

    double kkx = kx[ik];
    double kky = ky[ik];
    
    double wk =kkx*kkx + kky*kky;
    double wki = -1.0/(max(wk,0.001));
    
    UX[ik].x =  kkx*(kky*ZK[ik].x+kkx*GK[ik].x)*wki;
    UX[ik].y =  kkx*(kky*ZK[ik].y+kkx*GK[ik].y)*wki;

    UY[ik].x =  kky*(kky*ZK[ik].x+kkx*GK[ik].x)*wki;
    UY[ik].y =  kky*(kky*ZK[ik].y+kkx*GK[ik].y)*wki;
    
    VX[ik].x =  kkx*(kky*GK[ik].x-kkx*ZK[ik].x)*wki;
    VX[ik].y =  kkx*(kky*GK[ik].y-kkx*ZK[ik].y)*wki;

    VY[ik].x =  kky*(kky*GK[ik].x-kkx*ZK[ik].x)*wki;
    VY[ik].y =  kky*(kky*GK[ik].y-kkx*ZK[ik].y)*wki;

   ik += blockDim.x*gridDim.x;
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void normFF1(cufftDoubleComplex *NZK,cufftDoubleComplex *G2K, cufftDoubleComplex *F1, cufftDoubleComplex *F2, int *ikN){
  int ik = threadIdx.x + blockIdx.x*blockDim.x;
  //normalise and unpack (batched) D2Z FFT output
  while(ik < d_OK){
    
    int ikk = ikN[ik];

    G2K[ik].x = F2[ikk].x*d_IN;
    G2K[ik].y = F2[ikk].y*d_IN;
      
    NZK[ik].x = F1[ikk].x*d_IN;
    NZK[ik].y = F1[ikk].y*d_IN;

    NZK[ik+d_OK].x = F1[ikk+d_O2].x*d_IN;
    NZK[ik+d_OK].y = F1[ikk+d_O2].y*d_IN;

    NZK[ik+2*d_OK].x = F1[ikk+2*d_O2].x*d_IN;
    NZK[ik+2*d_OK].y = F1[ikk+2*d_O2].y*d_IN;

    NZK[ik+3*d_OK].x = F1[ikk+3*d_O2].x*d_IN;
    NZK[ik+3*d_OK].y = F1[ikk+3*d_O2].y*d_IN;

      ik += blockDim.x*gridDim.x;
  }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void normFFZ(cufftDoubleComplex *F2, cufftDoubleComplex *F1, int *ikN){
  int ik = threadIdx.x + blockIdx.x*blockDim.x;

  //normalise and unpack (single) D2Z FFT output
  while(ik < d_OK){

    int ikk = ikN[ik];
    F2[ik].x = F1[ikk].x*d_IN;
    F2[ik].y = F1[ikk].y*d_IN;
    ik += blockDim.x*gridDim.x;
  }
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void absReal(cufftDoubleReal* Z, cufftDoubleReal* NZ){
  int ik = threadIdx.x + blockIdx.x*blockDim.x;
  // write a temporary absolute value for getting the max

  while(ik < d_OR){
    Z[ik] = abs(NZ[ik]);
    ik += blockDim.x*gridDim.x;
  }

  
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void SqrReal(cufftDoubleReal* Z, cufftDoubleReal* NZ){
  int ik = threadIdx.x + blockIdx.x*blockDim.x;
  // do physical space multiplication

  while(ik < d_OR){
      Z[ik] = NZ[ik]*NZ[ik];
    ik += blockDim.x*gridDim.x;
  }

  
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void convol(cufftDoubleReal* G2R,cufftDoubleReal* NZR,double* avG2){
  int ik = threadIdx.x + blockIdx.x*blockDim.x;
  // do physical space convolution G2R are lambda terms in gamma^2
  // NZR has u*g and u*z

  while(ik < d_OR){
    double g = NZR[ik];
    double z = NZR[ik+d_OR];
    double u = NZR[ik+2*d_OR];
    double v = NZR[ik+3*d_OR];

    G2R[ik] = (2.0+d_LAM)*(*avG2-G2R[ik]);
    NZR[ik] = u*g;
    NZR[ik+d_OR] = v*g;
    NZR[ik+2*d_OR] = u*z;
    NZR[ik+3*d_OR] = v*z;

    ik += blockDim.x*gridDim.x;
  }

  
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void normalise(cufftDoubleReal* NZR){
  int ik = threadIdx.x + blockIdx.x*blockDim.x;
  // do normalisation of all the fields for the mapping to regular system

  while(ik < d_OR){
    double g = NZR[ik];
    double z = NZR[ik+d_OR];
    double u = NZR[ik+2*d_OR];
    double v = NZR[ik+3*d_OR];

    NZR[ik] = g/abs(d_MAX);
    NZR[ik+d_OR] = z/abs(d_MAX);
    NZR[ik+2*d_OR] = u/abs(d_MAX);
    NZR[ik+3*d_OR] = v/abs(d_MAX);

    ik += blockDim.x*gridDim.x;
  }

  
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void normaliseK(cufftDoubleComplex *Z,cufftDoubleComplex *Z0,cufftDoubleComplex *G,cufftDoubleComplex *G0){
  int ik = threadIdx.x + blockIdx.x*blockDim.x;
  // do normalisation of all the fields for the mapping to regular system
  // These are the initial arrays for RK4

  while(ik < d_OK){
    Z[ik].x=Z0[ik].x/abs(d_MAX);
    Z[ik].y=Z0[ik].y/abs(d_MAX);

    G[ik].x=G0[ik].x/abs(d_MAX);
    G[ik].y=G0[ik].y/abs(d_MAX);

    ik += blockDim.x*gridDim.x;
  }

  
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void Step1(cufftDoubleComplex *Z,cufftDoubleComplex *G,cufftDoubleComplex *Z0,cufftDoubleComplex *G0,cufftDoubleComplex *Z1,cufftDoubleComplex *G1,cufftDoubleComplex *NZK,cufftDoubleComplex *UK,cufftDoubleComplex *VK,double *kx,double *ky,double *avg2){
  int ik = threadIdx.x + blockIdx.x*blockDim.x;
  
  //This kernel performs the 1st and 2nd stage of RK4 and resets (U,V)
  //NB input arguments change between 1st and second call
  while(ik < d_OK){

      double kky = ky[ik];
      double kkx = kx[ik];
      double wk  = kkx*kkx + kky*kky;
      double wki = 1.0/(max(wk,0.001)); 
      // doing dealiasing mask here to save memory and fit N=4096 on FIONN
      double LM = exp(-36.0*(pow(2*sqrt(wk)/double(d_NX),36.0))); 
      // extra drag term required for the mapping
      double term = sgn(d_MAX)*(1+d_LAM-(2+d_LAM)*(*avg2));//abs(d_MAX);

      // Do final derivatives for nonlinear terms i.e. -div(U*G) and -div(U*Z)
      double NZKX = LM*( kkx*NZK[ik+2*d_OK].y + kky*NZK[ik+3*d_OK].y)+term*Z0[ik].x;
      double NZKY = LM*(-kkx*NZK[ik+2*d_OK].x - kky*NZK[ik+3*d_OK].x)+term*Z0[ik].y;

      double NGKX = LM*( kkx*NZK[ik].y + kky*NZK[ik+d_OK].y + UK[ik].x)+term*G0[ik].x;
      double NGKY = LM*(-kkx*NZK[ik].x - kky*NZK[ik+d_OK].x + UK[ik].y)+term*G0[ik].y;

      Z1[ik].x = d_DELTA*NZKX;
      Z1[ik].y = d_DELTA*NZKY;

      G1[ik].x = d_DELTA*NGKX;
      G1[ik].y = d_DELTA*NGKY;

      Z0[ik].x = Z[ik].x+0.5*d_DELTA*NZKX;
      Z0[ik].y = Z[ik].y+0.5*d_DELTA*NZKY;

      G0[ik].x = G[ik].x+0.5*d_DELTA*NGKX;
      G0[ik].y = G[ik].y+0.5*d_DELTA*NGKY;

      // Update spectral velocity coeffs
      UK[ik].x = -(kky*Z0[ik].y+kkx*G0[ik].y)*wki;
      UK[ik].y =  (kky*Z0[ik].x+kkx*G0[ik].x)*wki;
      
      VK[ik].x =  (kkx*Z0[ik].y-kky*G0[ik].y)*wki;
      VK[ik].y = -(kkx*Z0[ik].x-kky*G0[ik].x)*wki;
      // Update vorticity array

      // reset this array
      NZK[ik].x = 0.0;
      NZK[ik].y = 0.0;
      NZK[ik+d_OK].x = 0.0;
      NZK[ik+d_OK].y = 0.0;
      NZK[ik+2*d_OK].x = 0.0;
      NZK[ik+2*d_OK].y = 0.0;
      NZK[ik+3*d_OK].x = 0.0;
      NZK[ik+3*d_OK].y = 0.0;

    ik+=blockDim.x*gridDim.x;
    
  }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void Step2(cufftDoubleComplex *Z,cufftDoubleComplex *G,cufftDoubleComplex *Z0,cufftDoubleComplex *G0,cufftDoubleComplex *Z1,cufftDoubleComplex *G1,cufftDoubleComplex *NZK,cufftDoubleComplex *UK,cufftDoubleComplex *VK,double *kx,double *ky,double *avg2){
  int ik = threadIdx.x + blockIdx.x*blockDim.x;
  
  //This kernel performs the 3rd stage of RK4 and resets (U,V)
  while(ik < d_OK){

      double kky = ky[ik];
      double kkx = kx[ik];
      double wk  = kkx*kkx + kky*kky;
      double wki = 1.0/(max(wk,0.001));
      // doing dealiasing mask here to save memory and fit N=4096 on FIONN
      double LM = exp(-36.0*(pow(2*sqrt(wk)/double(d_NX),36.0)));
      // extra drag term required for the mapping
      double term = sgn(d_MAX)*(1+d_LAM-(2+d_LAM)*(*avg2));//abs(d_MAX);
 
      // Do final derivatives for nonlinear terms i.e. -div(U*G) and -div(U*Z)
      double NZKX = LM*( kkx*NZK[ik+2*d_OK].y + kky*NZK[ik+3*d_OK].y)+term*Z0[ik].x;
      double NZKY = LM*(-kkx*NZK[ik+2*d_OK].x - kky*NZK[ik+3*d_OK].x)+term*Z0[ik].y;

      double NGKX = LM*( kkx*NZK[ik].y + kky*NZK[ik+d_OK].y + UK[ik].x)+term*G0[ik].x;
      double NGKY = LM*(-kkx*NZK[ik].x - kky*NZK[ik+d_OK].x + UK[ik].y)+term*G0[ik].y;
 
      Z1[ik].x = d_DELTA*NZKX;
      Z1[ik].y = d_DELTA*NZKY;

      G1[ik].x = d_DELTA*NGKX;
      G1[ik].y = d_DELTA*NGKY;

      Z0[ik].x = Z[ik].x+d_DELTA*NZKX;
      Z0[ik].y = Z[ik].y+d_DELTA*NZKY;

      G0[ik].x = G[ik].x+d_DELTA*NGKX;
      G0[ik].y = G[ik].y+d_DELTA*NGKY;

      // Update spectral velocity coeffs
      UK[ik].x = -(kky*Z0[ik].y+kkx*G0[ik].y)*wki;
      UK[ik].y =  (kky*Z0[ik].x+kkx*G0[ik].x)*wki;
      
      VK[ik].x =  (kkx*Z0[ik].y-kky*G0[ik].y)*wki;
      VK[ik].y = -(kkx*Z0[ik].x-kky*G0[ik].x)*wki;
      // Update vorticity array

    // reset this array
      NZK[ik].x = 0.0;
      NZK[ik].y = 0.0;
      NZK[ik+d_OK].x = 0.0;
      NZK[ik+d_OK].y = 0.0;
      NZK[ik+2*d_OK].x = 0.0;
      NZK[ik+2*d_OK].y = 0.0;
      NZK[ik+3*d_OK].x = 0.0;
      NZK[ik+3*d_OK].y = 0.0;

    ik+=blockDim.x*gridDim.x;
    
  }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void Step(cufftDoubleComplex *Z,cufftDoubleComplex *G,cufftDoubleComplex *Z0,cufftDoubleComplex *G0,cufftDoubleComplex *Z1,cufftDoubleComplex *G1,cufftDoubleComplex *Z2,cufftDoubleComplex *G2,cufftDoubleComplex *Z3,cufftDoubleComplex *G3,cufftDoubleComplex *NZK,cufftDoubleComplex *UK,cufftDoubleComplex *VK,double *kx,double *ky,double *avg2){
  int ik = threadIdx.x + blockIdx.x*blockDim.x;
  
  //This kernel performs the final stage of RK4 and reset (U,V)
  while(ik < d_OK){

    
      double kky = ky[ik];
      double kkx = kx[ik];
      double wk  = kkx*kkx + kky*kky;
      double wki = 1.0/(max(wk,0.001));
      // doing dealiasing mask here to save memory and fit N=4096 on FIONN
      double LM = exp(-36.0*(pow(2*sqrt(wk)/double(d_NX),36.0)));
      // extra drag term required for the mapping
      double term = sgn(d_MAX)*(1+d_LAM-(2+d_LAM)*(*avg2));//abs(d_MAX);
      // Apply hyperviscosity using Crank-Nicholson scheme for stability
      double damp1 = 1.0;
      double damp2 = 1.0;
      if(sqrt(wk) > 200){
	damp1 = (1.-0.5*d_DELTA*d_NU*wk*wk);
	damp2 = (1.+0.5*d_DELTA*d_NU*wk*wk);
      }
 
      // Do final derivatives for nonlinear terms i.e. -div(U*G) and -div(U*Z)
      double NZKX = LM*( kkx*NZK[ik+2*d_OK].y + kky*NZK[ik+3*d_OK].y)+term*Z0[ik].x;
      double NZKY = LM*(-kkx*NZK[ik+2*d_OK].x - kky*NZK[ik+3*d_OK].x)+term*Z0[ik].y;

      double NGKX = LM*( kkx*NZK[ik].y + kky*NZK[ik+d_OK].y + UK[ik].x)+term*G0[ik].x;
      double NGKY = LM*(-kkx*NZK[ik].x - kky*NZK[ik+d_OK].x + UK[ik].y)+term*G0[ik].y;
 
      Z0[ik].x = (damp1*Z[ik].x+sixth*(Z1[ik].x+d_DELTA*NZKX) + third*(Z2[ik].x+Z3[ik].x))/damp2;
      Z0[ik].y = (damp1*Z[ik].y+sixth*(Z1[ik].y+d_DELTA*NZKY) + third*(Z2[ik].y+Z3[ik].y))/damp2;

      G0[ik].x = (damp1*G[ik].x+sixth*(G1[ik].x+d_DELTA*NGKX) + third*(G2[ik].x+G3[ik].x))/damp2;
      G0[ik].y = (damp1*G[ik].y+sixth*(G1[ik].y+d_DELTA*NGKY) + third*(G2[ik].y+G3[ik].y))/damp2;

      // Update spectral velocity coeffs
      UK[ik].x = -(kky*Z0[ik].y+kkx*G0[ik].y)*wki;
      UK[ik].y =  (kky*Z0[ik].x+kkx*G0[ik].x)*wki;
      
      VK[ik].x =  (kkx*Z0[ik].y-kky*G0[ik].y)*wki;
      VK[ik].y = -(kkx*Z0[ik].x-kky*G0[ik].x)*wki;
      // Update vorticity array

    // reset this array
      NZK[ik].x = 0.0;
      NZK[ik].y = 0.0;
      NZK[ik+d_OK].x = 0.0;
      NZK[ik+d_OK].y = 0.0;
      NZK[ik+2*d_OK].x = 0.0;
      NZK[ik+2*d_OK].y = 0.0;
      NZK[ik+3*d_OK].x = 0.0;
      NZK[ik+3*d_OK].y = 0.0;

    ik+=blockDim.x*gridDim.x;
    
  }
}
