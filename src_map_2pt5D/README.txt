

		GPU Pseudospectral solver for the MAPPED 2.5D model of the 3D Euler equations presented in the paper

		"Symmetry-plane model of 3D Euler flows and mapping to regular systems to improve blowup
		 assessment using numerical and analytical solutions" Mulungye, Lucas and Bustamante 2014.


____________________________________________________________________________________________________________________________


Outline of the code:

	   Code is of a standard pseudospectral form where timestepping occurs on fourier coefficients 
	   and nonlinear convolutions are computed in physical space. Timestepping scheme is RK4
	   (fourth order Runge-Kutta). N.B. normalisation and additional terms are included for the
	   mapping and analytic solutions are in tau.

	   Code is for use on NVIDIA GPUs via the CUDA programming syntax.
	   Use is made of the CUFFT library for the Fast Fourier Transform (FFT) on GPUs. We also
	   make use of the Thrust library of CUDA kernels for global reductions (spatial averages
	   and maxima). 

Inputs required:
	 
	    Initial data is set as in the reference above, user specifies the following inside params.txt:
	    '(NX,NY)' 	 resolution (max 4096^2 on Fermi M2090 cards and Kepler K20 cards has been tested) 
	    'Tstep'   	 duration of simulation 
	    'delta'   	 timestep
	    'TOut'	 write interval, note this is nearest t intervals not timestep multiples (see below) 

	    If the case lambda=0 is considered, the files "t_tau_lambda0.txt" and "G_tau_lambda0.txt" are
	    read at the start of the simulation. These files contain 'exact' solutions for computing error;
	    in fact they come from numerically integrating the ODE for S to get SUP(gamma).

Data is output in 3 files (named with resolution):

	    stats*.dat contains the t, CPU time, maximum stretching (gamma), vorticity (omega) at the position of maximum gamma,
	    	        average of grad gamma squared <grad g ^2>, in integral of the average of gamma squared <g^2>,
			 <g^2> itself and Tstar estimate
	    err*.dat   contains t, CPU time, relative error of max(g), relative error of omega at max(g), relative error of <g^2>,
	    	       relative error in Tstar, Q_gamma and Q_omega
		       Note these errors are for the cases lambda=-3/2 and lambda=0 only, other cases require adding 
		       in the source code. For lambda=0 the asymptotic solution is used for comparison via the gsl Lambert function.
		       See code for the details of which errors are meaningful where/when.

	    spec*.dat  contains a time series of the 1D spectrum of gamma.

	    vort*.dat  is a binary file containing the physical fields of gamma and omega at given write intervals.
	    	       This data can be read and rewritten using the fortran routine rewrite_vort.f where the user specifies
	     	       the file, resolution and frame of interest. Movies may then be created via make_movie bash script 
		       and gnuplot (see comments therein).


  -----------
  Contacts
  -----------

        Authors: Dan Lucas
        Institution: School of Mathematical Sciences, University College Dublin
        Email:  dan.lucas@ucd.ie
	
11/09/14
